source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'paypal-sdk-rest'
gem 'stripe'

# From rails core
gem 'rails', '~> 5.0.1'
gem 'pg'
gem 'puma'
gem 'rack-cors'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'jbuilder'
# Use Puma as the app server in development

# Core
gem 'will_paginate'
gem 'country_select'
gem 'delayed_job_active_record'
gem 'daemons'
gem 'rest-client'
gem 'erubis'

# UI
gem 'haml'
gem 'paperclip'
gem 'jquery-rails'
gem 'simple_form'
gem 'coffee-script'
gem 'active_link_to'
gem 'will_paginate-bootstrap'
gem 'bootstrap-colorpicker-rails'
gem 'bootstrap-table-rails'

## Authentication
gem 'oauth2'
gem 'sorcery'
gem 'bcrypt', '~> 3.1.2'
gem 'bcrypt-ruby'

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'

  # Capistrano deploy
  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-bundler'
  gem 'capistrano-passenger'
  gem 'capistrano-linked-files'
  gem 'capistrano-rvm'
  gem "capistrano-db-tasks", require: false

  gem 'letter_opener'
  gem 'annotate'
end

group :test do
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'rspec-rails'
  gem 'timecop'
end

group :development, :test do
  gem 'faker'
  gem 'pry'
  gem 'pry-nav'
  gem 'pry-rails'
end
