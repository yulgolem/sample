$(document).ready(function () {

  var product_id = $('#product_id').val();

  var path = '/admin/products/'+ product_id + '/upload_attachment'

  console.log(path)

  var txtFileUpload = $("#txtUpload").uploadFile({
    url: path,
    method: 'PUT',
    multiple: false,
    fileName: "product[txt_list]",
    autoSubmit: false,
    showCancel: true,
    showAbort: false,
    dragDrop: false,
    maxFileCount: 1,
    showPreview: false,
    showProgress: true,
    showStatusAfterSuccess: true
  });

  var xlsFileUpload = $("#xlsUpload").uploadFile({
    url: path,
    method: 'PUT',
    multiple: false,
    fileName: "product[xls_list]",
    autoSubmit: false,
    showCancel: true,
    showAbort: false,
    dragDrop: false,
    maxFileCount: 1,
    showPreview: false,
    showProgress: true,
    showStatusAfterSuccess: true
  });

  var csvFileUpload = $("#csvUpload").uploadFile({
    url: path,
    method: 'PUT',
    multiple: false,
    fileName: "product[csv_list]",
    autoSubmit: false,
    showCancel: true,
    showAbort: false,
    dragDrop: false,
    maxFileCount: 1,
    showPreview: false,
    showProgress: true,
    showStatusAfterSuccess: true
  });

  var exampleFileUpload = $("#exampleUpload").uploadFile({
    url: path,
    method: 'PUT',
    multiple: false,
    fileName: "product[example_list]",
    autoSubmit: false,
    showCancel: true,
    showAbort: false,
    dragDrop: false,
    maxFileCount: 1,
    showPreview: false,
    showProgress: true,
    showStatusAfterSuccess: true
  });

  $("#save").click(function (e) {
    $("#save").prop("disabled", true);
    txtFileUpload.startUpload();
    xlsFileUpload.startUpload();
    csvFileUpload.startUpload();
    exampleFileUpload.startUpload();
    e.preventDefault();
  });


});
