function handleFiles(add_to_element, files) {
  add_to_element.html("");
  for (var i = 0; i < files.length; i++) {
    var file = files[i];
    var imageType = /^image\//;

    if (!imageType.test(file.type)) {
      continue;
    }

    add_to_element[0].classList.add("image_for_upload_wrapper");

    var img = document.createElement("img");
    img.classList.add("for-upload");
    img.file = file;

    $(add_to_element).append(img);


    var reader = new FileReader();
    reader.onload = (function (aImg) {
      return function (e) { aImg.src = e.target.result; }; })(img);
    reader.readAsDataURL(file);
  }
}

$(document).ready(function () {
  $("#upload_image").on("change", function () {
    file_list = this.files;
    handleFiles($("#image_holder"), file_list)
  })

  // $(".remove_image_button").on("click", function () {
  //   var parent = $(this).parent();
  //   var remove_checkbox = parent.find("input[type=checkbox]");
  //   remove_checkbox.attr("checked", true);
  //   parent.addClass("hidden");
  // })

})
