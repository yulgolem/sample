$(document).ready(function() {
  $('.payment-variant').on('click', function(e) {
    var variant = e.target.value;
    $('.payment-details').addClass('hidden')
    if(variant == 'paypal') {
      $('.payment-details.paypal').removeClass('hidden')
    }
    if(variant == 'card') {
      $('.payment-details.card').removeClass('hidden')
    }
  })
});
