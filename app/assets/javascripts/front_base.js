//= require for_front/modernizr/modernizr.min.js
//= require for_front/jquery/jquery.min.js
//= require for_front/jquery.appear/jquery.appear.min.js
//= require for_front/jquery.easing/jquery.easing.min.js
//= require for_front/jquery-cookie/jquery-cookie.min.js
//= require for_front/bootstrap/js/bootstrap.min.js
//= require for_front/common/common.min.js
//= require for_front/jquery.validation/jquery.validation.min.js
//= require for_front/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js
//= require for_front/jquery.gmap/jquery.gmap.min.js
//= require for_front/jquery.lazyload/jquery.lazyload.min.js
//= require for_front/isotope/jquery.isotope.min.js
//= require for_front/owl.carousel/owl.carousel.min.js
//= require for_front/magnific-popup/jquery.magnific-popup.min.js
//= require for_front/vide/vide.min.js
//= require front_theme/theme.js
//= require for_front/rs-plugin/js/jquery.themepunch.tools.min.js
//= require for_front/rs-plugin/js/jquery.themepunch.revolution.min.js
//= require for_front/circle-flip-slideshow/js/jquery.flipshow.min.js
//= require front_theme/views/view.home.js
//= require front_theme/custom.js
//= require front_theme/theme.init.js
//= require front_theme/examples/examples.demos.js
//= require for_front/notify/notify.min.js

//= require jquery_ujs

//= require_self

//= require front/cart.js
//= require front/products.js
//= require front/stripe.js

$.notify.addStyle('foo-error', {
  html:
    "<div>" +
      "<div class='alert alert-danger alert-dismissible no_margin' role='alert'>"+
        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"+
        "<div>"+
          "<i class='fa fa-warning'></i>" +
          "<span class='title' data-notify-html='title'/>"+
        "</div>"+
      "</div>"+
    "</div>"
});

$.notify.addStyle('foo-success', {
  html:
    "<div>" +
      "<div class='alert alert-success alert-dismissible no_margin' role='alert'>"+
        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"+
        "<div>"+
          "<i class='fa fa-check-square'></i>" +
          "<span class='title' data-notify-html='title'/>"+
        "</div>"+
      "</div>"+
    "</div>"
});

$(".notify-box").each(function(i, e){
  $.notify(
    {
      title: $(e).data("message")
    },
    {
      style: "foo-"+$(e).data("class"),
      position:"top center",
      clickToHide: false,
      autoHideDelay: 10000,
      autoHide: true
    }
  );
});
