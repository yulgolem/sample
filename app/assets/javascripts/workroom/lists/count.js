var refreshState = null;

$(document).ready(function(){
  $('#check_count').on("click", function(event) {
    updateCount();
  });
});

function updateCount(){
  $.ajax({
  url:  "/workroom/lists/check_company_count",
  dataType: 'json',
  data:{
    orb_list: {
      title: $("#orb_list_title").val(),
      orb_num: $("#orb_list_orb_num").val(),
      entity_type: $("#orb_list_entity_type").val(),
      name: $("#orb_list_name").val(),
      name_prefix: $("#orb_list_name_prefix").val(),
      webdomain: $("#orb_list_webdomain").val(),
      industry: $("#orb_list_industry").val(),
      naics_codes: $("#orb_list_naics_codes").val(),
      sic_codes: $("#orb_list_sic_codes").val(),
      employees: $("#orb_list_employees").val(),
      revenue: $("#orb_list_revenue").val(),
      city: $("#orb_list_city").val(),
      state: $("#orb_list_state").val(),
      zip: $("#orb_list_zip").val(),
      country: $("#orb_list_country").val(),
      parent_orb_num: $("#orb_list_parent_orb_num").val(),
      rankings: $("#orb_list_rankings").val(),
      technologies: $("#orb_list_technologies").val(),
      tech_categories: $("#orb_list_tech_categories").val(),
      importance_score: $("#orb_list_importance_score").val()
    }
        },
  type: "GET"
  }).done(function(data){
      $("#company_count").html("Company count: " + data.count);
});
}
