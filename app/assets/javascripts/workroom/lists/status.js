var refreshState = null;

$(document).ready(function(){
  refreshState = setInterval(updateStatus,3000);
});

function updateStatus(){
  $.ajax({
  url:  "/workroom/lists/state",
  dataType: 'json',
  type: "GET"
  }).done(function(data_array){
    data_array.forEach(function(data) {
      if (data.status == 'completed') {
        // clearInterval(refreshState);
        var count_id = "#result_"+data.id;
        var date_id = "#date_"+data.id;
        var progress_id = "#progress_"+data.id;
        var csv_id = "#download_csv_"+data.id;
        var excel_id = "#download_excel_"+data.id;
        $(count_id).html(data.count);
        $(progress_id).html('100%');
        $(date_id).html(data.date);
        event.preventDefault();
        $(csv_id).removeClass("disabled");
        $(excel_id).removeClass("disabled");
        }});
    // else if (data.status == 'pending')
    //   $("#status").html('Data is updating.');
});
}
