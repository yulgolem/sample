var refreshState = null;

$(document).ready(function(){
  refreshState = setInterval(updateStatus,3000);
});

function updateStatus(){
  $.ajax({
  url:  "/workroom/naics/state",
  dataType: 'json',
  type: "GET"
  }).done(function(data){
    if (data.status == 'completed') {
      $("#status").html('Last update at ' + data.updated);
      }
    else if (data.status == 'pending')
      $("#status").html('Data is updating.');
});
}
