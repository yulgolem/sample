var refreshState = null;

$(document).ready(function(){
  refreshState = setInterval(updateStatus,10000);
});

function updateStatus(){
  $.ajax({
  url:  "/workroom/technology_categories/state",
  dataType: 'json',
  type: "GET"
  }).done(function(data){
    if (data.status == 'completed') {
      clearInterval(refreshState);
      $("#status").html('Data is up to date.');
      }
    else if (data.status == 'pending')
      $("#status").html('Data is updating.');
});
}
