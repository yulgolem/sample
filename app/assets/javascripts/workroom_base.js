//= require for_admin/modernizr/modernizr.js
//= require for_admin/jquery/jquery.js
//= require for_admin/jquery-browser-mobile/jquery.browser.mobile.js
//= require for_admin/bootstrap/js/bootstrap.js
//= require for_admin/nanoscroller/nanoscroller.js
//= require for_admin/bootstrap-datepicker/js/bootstrap-datepicker.js
//= require for_admin/magnific-popup/jquery.magnific-popup.js
//= require for_admin/jquery-placeholder/jquery-placeholder.js
//= require for_admin/jquery-ui/jquery-ui.js
//= require for_admin/jqueryui-touch-punch/jqueryui-touch-punch.js

//= require for_admin/select2/js/select2.js
//= require for_admin/jquery-appear/jquery-appear.js
//= require for_admin/bootstrap-multiselect/bootstrap-multiselect.js
//= require for_admin/jquery.easy-pie-chart/jquery.easy-pie-chart.js
//= require for_admin/flot/jquery.flot.js
//= require for_admin/flot.tooltip/flot.tooltip.js
//= require for_admin/flot/jquery.flot.pie.js
//= require for_admin/flot/jquery.flot.categories.js

//= require for_admin/flot/jquery.flot.resize.js
//= require for_admin/jquery-sparkline/jquery-sparkline.js
//= require for_admin/raphael/raphael.js
//= require for_admin/morris.js/morris.js
//= require for_admin/gauge/gauge.js
//= require for_admin/snap.svg/snap.js
//= require for_admin/liquid-meter/liquid.meter.js
//= require for_admin/jqvmap/jquery.vmap.js
//= require for_admin/jqvmap/data/jquery.vmap.sampledata.js
//= require for_admin/jqvmap/maps/jquery.vmap.world.js
//= require for_admin/jqvmap/maps/continents/jquery.vmap.africa.js
//= require for_admin/jqvmap/maps/continents/jquery.vmap.asia.js
//= require for_admin/jqvmap/maps/continents/jquery.vmap.australia.js
//= require for_admin/jqvmap/maps/continents/jquery.vmap.europe.js
//= require for_admin/jqvmap/maps/continents/jquery.vmap.north-america.js
//= require for_admin/jqvmap/maps/continents/jquery.vmap.south-america.js
//= require for_admin/autosize/autosize.js
//= require for_admin/bootstrap-fileupload/bootstrap-fileupload.min.js

//= require admin_theme/theme.js
//= require admin_theme/theme.custom.js
//= require admin_theme/theme.init.js

//= require jquery_ujs
//= require bootstrap-colorpicker

//= require admin/_images_upload.js

//= require_self

$(document).ready(function () {
  initAdminFormHandler();
  $('.colorpicker').colorpicker()
});

// global: error toggler
function error($title, $field, error, toggle) {
  var $title__error = $title.find('.error');

  if (toggle) {
    $title__error.remove();
    $field.removeClass('error__field');
  } else {
    if (!$field.hasClass('error__field')) {
      $($title).append($('<span/>', {
        class: 'error',
        text: error,
      }).css({ 'font-weight': '700' }).delay(100).animate({ 'font-weight': '400' }, 'slow'))
      $field.addClass('error__field');
    }
  }
}


function initAdminFormHandler() {
  console.log("Asd")
  var form = document.forms.namedItem("admin_form");
  if (form) {
    form.addEventListener('submit', function (ev) {
      var submit = form.getElementsByClassName('submit-all');
      $(submit)
        .data('loading-text', 'Loading...')
        .button('loading');

      // if (tinymce !== undefined) {
      //   tinymce.triggerSave();
      // }

      var oData = new FormData(form);

      // check if there any dropzones for project
      var _dropzone = form.getElementsByClassName("_dropzone");
      for (var i = 0; i < _dropzone.length; i++) {
        if (_dropzone[i] && _dropzone[i].dropzone != undefined && _dropzone[i].dropzone.files.length != 0) {
          _dropzone[i].dropzone.files.forEach(function (file) {
            var uploadNmae = $(_dropzone[i]).data("upload-name")
            oData.append(uploadNmae, file, file.name);
          })
        }
      }

      var oReq = new XMLHttpRequest();

      var httpMethod = $(form).find("input[name=_method]").attr("value") || form.getAttribute("method")
      oReq.open(httpMethod, form.getAttribute("action"), true);

      oReq.send(oData);

      oReq.onload = function (oEvent) {
        // console.log(oReq.responseText);

        if (oReq.status == 200) {
          var response = JSON.parse(oReq.responseText);
          error($("[data-error-label]"), $("[data-error-input]"), "", true);

          if (response["is_valid"]) {
            if (response["redirect_url"]) {
              window.location.href = response["redirect_url"];
            }
          } else {
            var first_error_key = Object.keys(response["errors"])[0];
            $("html, body").animate({ scrollTop: $("[data-error-label=" + first_error_key + "]").offset().top }, 300)
            Object.keys(response["errors"]).forEach(function (key) {
              var value = response["errors"][key]
              error($("[data-error-label=" + key + "]"), $("[data-error-input=" + key + "]"), value[0], false);
            })
          }
          if (response["message"]) {
            displayBootstrapError(response["message"]);
          } else {
            $("#admin_notifications").html("");
          }
        } else {
          alert("Error occured! :<");
        }

        if (!response["redirect_url"]) {
          $(submit).button('reset');
        }
      };
      ev.preventDefault();
    }, false);
  }
}
