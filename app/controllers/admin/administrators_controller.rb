class Admin::AdministratorsController < Admin::BaseController
  def index
    @users = User.where(role: 'admin')
  end
end
