class Admin::BaseController < ApplicationController
  layout "admin_base"

  before_filter :check_admin_auth

  def check_admin_auth
    if !(current_user && ['master', 'admin'].include?(current_user.role))
      redirect_to admin_sign_in_url
    end
  end
end
