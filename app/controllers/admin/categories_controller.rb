class Admin::CategoriesController < Admin::BaseController

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @categories = Category.all
    @categories = @categories.paginate(page: page, per_page: per_page)
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.valid?
      @category.save
      flash[:notice] = 'Category created'
      redirect_to admin_categories_url(category_id: @category.id)
    else
      render action: :new
    end
  end

  def edit
    @category = Category.find_by(id: params[:id])
    if @category
      render
    else
      flash[:error] = 'Category not found'
      redirect_to admin_categories_url
    end
  end

  def update
    @category = Category.find_by(id: params[:id])
    if @category
      @category.assign_attributes(category_params)
      if @category.valid?
        @category.save
        flash[:notice] = 'Category updated'
        redirect_to admin_categories_url
      else
        render action: :edit
      end
    else
      flash[:error] = 'Category not found'
      redirect_to admin_categories_url
    end
  end

  def destroy
    @category = Category.find_by(id: params[:id])
    if @category
      if @category.allowed_to_destroy?
        @category.destroy
        flash[:notice] = "Category \'#{@category.title}\' destroyed"
      else
        flash[:error] = "Category \'#{@category.title}\' contains subcategories that attached to product(s), you can\'t delete this category"
      end
    else
      flash[:error] = "Category not found"
    end
    redirect_to admin_categories_url
  end

private
  
  def category_params
    params.require(:category).permit!
  end

end
