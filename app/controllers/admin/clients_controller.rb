class Admin::ClientsController < Admin::BaseController

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @users = User.where.not(role: 'admin').order("created_at DESC").paginate(page: page, per_page: per_page)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.valid?
      @user.skip_activation_email = true
      @user.save
      flash[:notice] = 'User created'
      redirect_to admin_clients_url
    else
      render action: :new
    end
  end

  def edit
    @user = User.find_by(id: params[:id])
    if @user
      render
    else
      flash[:error] = 'User not found'
      redirect_to admin_clients_url
    end
  end

  def update
    @user = User.find_by(id: params[:id])
    if @user
      @user.assign_attributes(user_params)
      if @user.valid?
        @user.skip_activation_email = true
        @user.save
        flash[:notice] = 'User updated'
        redirect_to admin_clients_url
      else
        render action: :edit
      end
    else
      flash[:error] = 'User not found'
      redirect_to admin_clients_url
    end
  end

  def destroy
    @user = User.find_by(id: params[:id])
    if @user
      @user.destroy
      flash[:notice] = 'User destroyed'
    else
      flash[:error] = 'User not found'
    end
    redirect_to admin_clients_url
  end

  def activate
    @user = User.find_by(id: params[:id])
    if @user
      @user.skip_activation_success_email = true
      @user.activate!
      flash[:notice] = 'User activated'
    else
      flash[:error] = 'User not found'
    end
    redirect_to admin_clients_url
  end

private

  def user_params
    password = params.require(:user)[:password]
    if password.present?
      params[:user][:password_confirmation] = password
    end
    params.require(:user).permit!
  end
end
