class Admin::ContactMessagesController < Admin::BaseController

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @contact_messages = ContactMessage.all.paginate(page: page, per_page: per_page)
    @global_setting = GlobalSetting.get_instance
  end

  def update_feedback_email
    @global_setting = GlobalSetting.get_instance
    @global_setting.assign_attributes(global_setting_params)
    if @global_setting.valid?
      @global_setting.save
      flash[:notice] = 'Feedback email updated'
      redirect_to admin_contact_messages_url
    else
      flash[:error] = 'Feedback email is invalid'
      redirect_to admin_contact_messages_url
    end
  end

  private

  def global_setting_params
    params.require(:global_setting).permit(:feedback_target_email)
  end
end
