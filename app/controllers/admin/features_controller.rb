class Admin::FeaturesController < Admin::BaseController

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @features = Feature.all.paginate(page: page, per_page: per_page)
  end

  def new
    @feature = Feature.new
  end

  def create
    @feature = Feature.new(feature_params)
    if @feature.valid?
      @feature.save
      flash[:notice] = 'Feature created'
      render json: { is_valid: true,  redirect_url: admin_features_url }
    else
      render json: { is_valid: false, errors: @feature.errors }
    end
  end

  def edit
    @feature = Feature.find_by(id: params[:id])
    if @feature
      render
    else
      flash[:error] = 'Feature not found'
      redirect_to admin_features_url
    end
  end

  def update
    @feature = Feature.find_by(id: params[:id])
    if @feature
      @feature.assign_attributes(feature_params)
      if @feature.valid?
        @feature.save
        flash[:notice] = 'Feature updated'
        render json: { is_valid: true,  redirect_url: admin_features_url }
      else
        render json: { is_valid: false, errors: @feature.errors }
      end
    else
      flash[:error] = 'Feature not found'
      redirect_to admin_features_url
    end
  end

  def destroy
    @feature = Feature.find_by(id: params[:id])
    if @feature
      @feature.destroy
      flash[:notice] = 'Feature destroyed'
    else
      flash[:error] = 'Feature not found'
    end
    redirect_to admin_features_url
  end

private

  def feature_params
    params.require(:feature).permit!
  end
end
