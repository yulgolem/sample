class Admin::OrdersController < Admin::BaseController

  def index
    @orders = Order.joins(:cart => :cart_products).includes(cart: :user).where('cart_products.id is not null').order('orders.id desc')
  end

  def show
    @order = Order.find_by(articulus: params[:articulus])
    unless @order.present?
      redirect_to front_404_url
    end
  end
end
