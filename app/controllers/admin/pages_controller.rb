class Admin::PagesController < Admin::BaseController

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @pages = Page.all.paginate(page: page, per_page: per_page)
  end

  def edit
    @page = Page.find_by(id: params[:id])
    if @page
      render
    else
      flash[:error] = 'Page not found'
      redirect_to admin_pages_url
    end
  end

  def update
    @page = Page.find_by(id: params[:id])
    if @page
      @page.assign_attributes(page_params)
      if @page.valid?
        @page.save
        flash[:notice] = 'Page updated'
        redirect_to admin_pages_url
      else
        render action: :edit
      end
    else
      flash[:error] = 'Page not found'
      redirect_to admin_pages_url
    end
  end

private

  def page_params
    params.require(:page).permit!
  end
end
