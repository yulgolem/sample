class Admin::ProductsController < Admin::BaseController

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @products = Product.all.order("updated_at DESC").paginate(page: page, per_page: per_page)
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)

    if @product.valid?
      @product.save
      flash[:notice] = 'Product created'
      render json: { is_valid: true,  redirect_url: admin_products_url }
    else
      p "Errors", @product.errors

      render json: { is_valid: false, errors: @product.errors }
    end
  end

  def edit
    @product = Product.includes(:subcategories, :features).find_by(id: params[:id])
    if @product
      render
    else
      flash[:error] = 'Product not found'
      redirect_to admin_products_url
    end
  end

  def update
    @product = Product.find_by(id: params[:id])
    if @product
      @product.assign_attributes(product_params)
      if @product.valid?
        @product.save
        flash[:notice] = 'Product updated'
        render json: { is_valid: true,  redirect_url: admin_products_url }
      else
        render json: { is_valid: false, errors: @product.errors }
      end
    else
      flash[:error] = 'Product not found'
      redirect_to admin_products_url
    end
  end

  def upload_attachment
    @product = Product.find(params[:id])
    if @product.update(product_params)
      render :text => 'ok'
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @product = Product.find_by(id: params[:id])
    if @product
      @product.destroy
      flash[:notice] = 'Product destroyed'
    else
      flash[:error] = 'Product not found'
    end
    redirect_to admin_products_url
  end

  def hide
    @product = Product.find_by(id: params[:id])
    if @product
      @product.hide!
      flash[:notice] = 'Product is now hidden and will not be shown on products page'
    else
      flash[:error]  = 'Product not found'
    end
    redirect_to admin_products_url
  end

  def unhide
    @product = Product.find_by(id: params[:id])
    if @product
      @product.show!
      flash[:notice] = 'Product is unhidden and will be shown on products page'
    else
      flash[:error] = 'Product not found'
    end
    redirect_to admin_products_url
  end

  def new_version
    edit()
  end

  def create_version
    @base_product = Product.find_by(id: params[:id])
    @product = @base_product.versionize_with(product_params)
    if @product.valid?
      @product.save
      flash[:notice] = 'Product created'
      render json: { is_valid: true,  redirect_url: admin_products_url }
    else
      render json: { is_valid: false, errors: @product.errors }
    end
  end

  def download_list
    @product = Product.find(params[:id])
    case params[:format]
    when "csv"
      send_file @product.csv_list.path, filename: "list_#{@product.articulus}.csv"
    when "xls"
      send_file @product.xls_list.path, filename: "list_#{@product.articulus}.xls"
    when "txt"
      send_file @product.txt_list.path, filename: "list_#{@product.articulus}.txt"
    end
  end

private

  def product_params
  #  dates_format = "%m/%d/%Y"
  #  last_updated_at_pub = params.require(:product)[:last_updated_at_pub]
  #  last_updated_at_internal = params.require(:product)[:last_updated_at_internal]
  #  params.require(:product)[:last_updated_at_pub] = Date.strptime(last_updated_at_pub, dates_format)
  #  params.require(:product)[:last_updated_at_internal] = Date.strptime(last_updated_at_internal, dates_format)
    params.require(:product).permit!
  end

end
