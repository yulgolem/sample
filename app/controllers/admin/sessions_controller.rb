class Admin::SessionsController < Admin::BaseController

  skip_before_filter :check_admin_auth, only: [:new, :create]

  def new
    if current_user && current_user.role == 'admin'
      redirect_to admin_root_url
    else
      @user = User.new
      render layout: "blank_layout"
    end
  end

  def create
    email = user_params[:email].downcase if user_params[:email]
    remember_me_flag = [1, true, '1', 'true'].include?(user_params[:remember])
    login_failed = false

    is_user_with_such_email_admin = User.find_by(email: email, role: 'admin').present?

    if is_user_with_such_email_admin
      user = login(email, user_params[:password], remember_me_flag)
      login_failed = true unless user
    else
      login_failed = true
    end

    if login_failed
      flash[:error] = 'Incorrect Login'
      redirect_to action: 'new'
    else
      flash[:notice] = 'Logged in successfully'
      redirect_to admin_root_url
    end
  end

  def destroy
    logout
    flash[:notice] = 'Logged out successfully'
    redirect_to admin_root_url
  end


  private

  def user_params
    params.require(:user).permit!
  end
end
