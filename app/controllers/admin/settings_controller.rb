class Admin::SettingsController < Admin::BaseController

  before_action :set_global_setting

  def index
  end

  def edit
  end

  def update
    if @global_setting
      @global_setting.assign_attributes(global_setting_params)
      if @global_setting.valid?
        @global_setting.save
        flash[:notice] = 'Settings updated'
        redirect_to admin_settings_url
      else
        render action: :edit
      end
    else
      flash[:error] = 'Settings not found'
      redirect_to admin_settings_url
    end
  end

  private

  def set_global_setting
    @global_setting = GlobalSetting.get_instance
  end

  def global_setting_params
    params.require(:global_setting).permit!
  end
end
