class Admin::SubcategoriesController < Admin::BaseController

  before_action :set_category, except: []

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @subcategories = @category.subcategories
    @subcategories = @subcategories.paginate(page: page, per_page: per_page)
  end

  def new
    @subcategory = @category.subcategories.new
  end

  def create
    @subcategory = @category.subcategories.new(subcategory_params)
    if @subcategory.valid?
      @subcategory.save
      flash[:notice] = 'Subcategory created'
      redirect_to admin_subcategories_url(category_id: @category.id)
    else
      render action: :new
    end
  end

  def edit
    @subcategory = Subcategory.find_by(id: params[:id])
    if @subcategory
      render
    else
      flash[:error] = 'Subcategory not found'
      redirect_to admin_subcategories_url(category_id: @category.id)
    end
  end

  def update
    @subcategory = Subcategory.find_by(id: params[:id])
    if @subcategory
      @subcategory.assign_attributes(subcategory_params)
      if @subcategory.valid?
        @subcategory.save
        flash[:notice] = 'Subcategory updated'
        redirect_to admin_subcategories_url(category_id: @category.id)
      else
        render action: :edit
      end
    else
      flash[:error] = 'Subcategory not found'
      redirect_to admin_subcategories_url(category_id: @category.id)
    end
  end

  def destroy
    @subcategory = Subcategory.find_by(id: params[:id])
    if @subcategory
      if @subcategory.allowed_to_destroy?
        @subcategory.destroy
        flash[:notice] = "Subcategory \'#{@subcategory.title}\' destroyed"
      else
        flash[:error] = "Subcategory \'#{@subcategory.title}\' contains attached to product(s), you can\'t delete this subcategory"
      end
    else
      flash[:error] = "Subcategory not found"
    end
    redirect_to admin_subcategories_url(category_id: @category.id)
  end

private

  def set_category
    @category = Category.find_by(id: params[:category_id])
    unless @category.present?
      flash[:error] = "No Category with such ID"
      redirect_to admin_categories_path
    end
  end

  def subcategory_params
    params.require(:subcategory).permit!
  end

end
