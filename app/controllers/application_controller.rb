class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_filter :staging_protection
  before_filter :set_page_metadata

  def set_page_metadata
    # Our ready-made marketing and industry companies lists: complete, accurate and ready for use
    @metadata = {
        meta_title: "Ready marketing and industry companies lists. Buy Listoteca lists to find new leads and accelerate your sales.",
        meta_desc: "",
        meta_keywords: ""
    }
  end

  def staging_protection
    return true if ['development', 'production'].include?(Rails.env)
    
    authenticate_or_request_with_http_basic('Listoteca access check') do |login, password|                                                                                              
      if (login == 'listoteca' && password == '$t@ging') then                                                                                                   
        true                                                                                                                                                  
      else                                                                                                                                                    
        false                                                                                                                                                 
      end                                                                                                                                                     
    end
  end
end
