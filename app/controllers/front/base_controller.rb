class Front::BaseController < ApplicationController
  layout "front_base"

  before_action :set_user_cart
  before_action :set_redirect_cookies

  def checkout
    if current_user
      redirect_to
    else
      redirect_to front_sign_in_path, notice: "Please, sign in before checkout"
    end
  end

  def set_user_cart
    if current_user
      @user_cart = current_user.cart
      @user_cart = current_user.carts.create unless @user_cart
    elsif cookies[:cart_id]
      @user_cart = Cart.find_by(id: cookies[:cart_id])
      @user_cart = Cart.create unless @user_cart
    else
      @user_cart = Cart.create
    end
    cookies[:cart_id] = @user_cart.id
    calculate_cart(@user_cart)
  end

  def calculate_cart(cart)
    cart_products = @user_cart.cart_products
    total_price = 0
    cart_products.each do |cart_product|
      total_price += cart_product.price.to_f
    end
    @user_cart.order = Order.create! unless @user_cart.order
    @user_cart.order.update_attribute(:total_price, total_price)
  end

  def check_auth
    redirect_to front_sign_in_path, notice: "Please, sign in" unless current_user
  end

  def set_redirect_cookies
    cookies[:redirect_back_to] = request.env['PATH_INFO']
  end

  def redirect_with_saved_cookies(path, options = {})
    if cookies[:redirect_back_to]
      redirect_to cookies[:redirect_back_to], options
    else
      redirect_back_or_to path, options
    end
  end


end
