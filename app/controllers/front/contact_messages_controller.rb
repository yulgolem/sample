class Front::ContactMessagesController < Front::BaseController

  def create
    @contact_message = ContactMessage.create(email: params[:email], name: params[:name], message: params[:message], user_id: current_user&.id)
    if @contact_message.valid?
      flash[:notice] = "Thanks! Your message has been successfully sent. We will contact you soon!"
      ContactMessagesMailer.delay.contact_message_email(@contact_message)
    else
      flash[:error] = "Sorry, error occurred"
    end
    redirect_to front_root_path
  end

end