class Front::ErrorsController < Front::BaseController
  def not_found
    render(:status => 404)
  end
end