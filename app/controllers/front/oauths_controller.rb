class Front::OauthsController < Front::BaseController
  skip_before_action :set_redirect_cookies

  def oauth
    login_at(params[:provider])
  end

  def callback
    provider = params[:provider]
    if params[:error]
      flash[:alert] = "Failed to login from #{provider.titleize}!"
      return redirect_with_saved_cookies(front_root_path)
    end
    if @user = login_from(provider)
      @user.process_latest_cart(@user_cart)
      flash[:notice] = "Logged in from #{provider.titleize}!"
      redirect_with_saved_cookies front_root_path
    else
      begin
        @user = create_from(provider)
        @user.process_latest_cart(@user_cart)
        @user.activate!
        # NOTE: this is the place to add '@user.activate!' if you are using user_activation submodule

        reset_session # protect from session fixation attack
        auto_login(@user)
        if(@user.email)
          flash[:notice] = "Logged in from #{provider.titleize}!"
          redirect_with_saved_cookies front_root_path
        else
          redirect_to front_profile_path
        end
      rescue
        flash[:alert] = "Failed to login from #{provider.titleize}!"
        redirect_with_saved_cookies front_root_path
      end
    end
  end
end
