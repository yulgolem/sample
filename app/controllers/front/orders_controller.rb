class Front::OrdersController < Front::BaseController
  before_action :check_auth, only: [:index]

  before_action :set_order,  only: [:show]

  skip_before_action :verify_authenticity_token
  skip_before_action :check_auth, only: [:notify]

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10
    @orders = current_user.orders.order('id desc').where('payment_token is not null').paginate(page: page, per_page: per_page)
    @orders_count =  current_user.orders.count
  end

  def stripe_success
    @order = Order.find_by(id: params['order']['id'])

    token = params[:stripeToken]

    charge = Stripe::Charge.create(
      :amount => @order.total_price.to_i * 100,
      :currency => "usd",
      :description => "Charge from Listoteca.com for order #{@order.articulus}",
      :source => token,
    )

    if charge.paid
      @order.update_attributes(status: 'paid', transaction_id: charge.id, payment_method: 'stripe')
      redirect_to :front_approved
    else
      redirect_to :front_rejected
    end
  rescue Exception => e
    if e.class == Stripe::CardError
      flash[:error] = e.message
      redirect_to front_cart_url
    end
  end

  def paypal_success
    @order  = Order.find_by(payment_token: params['paymentId'])
    @paypal = PaypalInterface.new(@order)
    response = @paypal.execute(params["PayerID"] )
    if response
      @order.update_attributes(status: 'paid', transaction_id: params['token'], payment_method: 'paypal')
      redirect_to :front_approved
    else
      redirect_to :front_rejected
    end
  end

  def approved
    cart = Cart.joins(:cart_products).where('carts.user_id = ?', current_user.id).where('cart_products.id is not null').last
    @order = cart.order

    unless @order.payment_approved
      approval = CheckPaymentStatusService.new(@order).execute
      @order.update_attribute(:payment_approved, approval)
    end

    @user_cart = Cart.create
    @user_cart.update_attribute(:user_id, current_user.id)
    @gtm_event = 'Transaction'
    cookies[:cart_id] = @user_cart.id
  end

  def rejected
    flash[:notice] = "Something went wrong, please, try again."
  end

  def notify
    render head :ok
    # order = Order.find_by(payment_token: params['pay_key'])
    # if params['.status'] == 'COMPLETED'
    #   order.update_attributes(status: 'paid', transaction_id: params['transaction']['0']['.id_for_sender_txn'])
    # else
    #   flash[:notice] = "Sorry, but the transaction was unsuccessfull: #{params['transaction']['.id_for_sender_txn']}"
    # end
  end

private
  def set_order
    @order = Order.find_by(articulus: params[:articulus])
    unless (@order.present? && @order.cart.user == current_user)
      redirect_to front_404_url
    end

    unless @order.payment_approved
      approval = Front::CheckPaymentStatusService.new(@order).execute
      @order.update_attribute(:payment_approved, approval)
    end
  end

  def order_params
    params.permit!
  end
end
