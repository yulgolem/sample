class Front::PagesController < Front::BaseController
  def welcome
    @metadata = Page.find_by(target: 'welcome') || @metadata
  end

  def faq
    @metadata = Page.find_by(target: 'faq') || @metadata
  end

  def about_us
    @metadata = Page.find_by(target: 'aboutus') || @metadata
    render :layout => "about_layout"
  end

  def privacy
    @metadata = Page.find_by(target: 'privacy') || @metadata
  end

  def terms_of_use
    @metadata = Page.find_by(target: 'terms_of_use') || @metadata
  end

  def dummy
    render layout: false
  end
end
