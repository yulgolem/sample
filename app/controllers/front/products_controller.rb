class Front::ProductsController < Front::BaseController
  before_action :check_auth, only: [:download_list]
  before_action :set_product, only: [:show, :download_example, :download_list, :add_to_cart, :remove_from_cart]


  # All code in init_list
  def index
    @categories    = Category.where(title: ['Industry', 'Geo'])

    @subcategories = params[:subcategories] || []
    @subcategories.map! { |s| s.to_i }

    @view          = params[:view] || 'list'

    @products      = Product.unhidden.latest_versions
    @products      = @products.filter_category(@subcategories) if @subcategories.present?
    @products      = @products.paginate(page: @page, per_page: @per_page)

    @metadata      = Page.find_by(target: 'readylists') || @metadata

    @page          = params[:page].try(:to_i)     || 1
    @per_page      = params[:per_page].try(:to_i) || 12

    render action: @view
  end


  def subcategory
    @subcategory   = params[:subcategory]
    @subcategories = params[:subcategories] || []
    if @subcategories.include?(@subcategory)
      @subcategories.reject!{ |s| s == @subcategory}
    else
      @subcategories << @subcategory
    end

    redirect_to front_products_path(subcategories: @subcategories)
  end

  def default_price_from_filter_value
    unless params[:product].blank?
      unless params[:product][:price_from].blank?
        params[:product][:price_from]
      end
    else
      nil
    end
  end

  def default_price_to_filter_value
    unless params[:product].blank?
      unless params[:product][:price_to].blank?
        params[:product][:price_to]
      end
    else
      nil
    end
  end

  def show
    @metadata = @product
    if @product.is_hidden
      flash[:error] = "Product not found"
      redirect_to front_root_url
    end
  end

  def download_example
    send_file @product.example_list.path
  end

  def download_list
    if current_user.bought_product?(@product.id)
      product_download = ProductDownload.find_or_create_by(user_id: current_user.id, product_id: @product.id, file_downloaded_at: Time.now)
      case params[:format]
      when "csv"
        product_download.add_csv
        send_file @product.csv_list.path, filename: "list_#{@product.articulus}.csv"
      when "xls"
        product_download.add_xls
        send_file @product.xls_list.path, filename: "list_#{@product.articulus}.xls"
      when "txt"
        product_download.add_txt
        send_file @product.txt_list.path, filename: "list_#{@product.articulus}.txt"
      end
    else
      redirect_to front_root_url, error: "Product is not bought yet"
    end
  end

  def add_to_cart
    unless @user_cart.cart_products.where(product_id: @product.id).any?
      price = @product.price_discounted || @product.price
      CartProduct.create(cart_id: @user_cart.id, product_id: @product.id, price: price)
    end
    redirect_to :back
  end

  def remove_from_cart
    if cart_product = CartProduct.find_by(cart_id: @user_cart.id, product_id:  @product.id)
      cart_product.destroy
    end
    redirect_to :back
  end

private

  def set_product
    @product = Product.find_by(articulus: params[:id])
    unless @product.present?
      redirect_to front_404_url
    end
  end

end
