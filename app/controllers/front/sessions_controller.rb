class Front::SessionsController < Front::BaseController
  skip_before_action :set_redirect_cookies
  
  def new
    @metadata = Page.find_by(target: 'sign_in') || @metadata
    if current_user
      redirect_with_saved_cookies front_root_path
    end
  end

  def create
    email = session_params[:email].downcase if session_params[:email]
    remember_me_flag = [1, true, '1', 'true'].include?(session_params[:remember])
    login_failed = false

    user = login(email, session_params[:password], remember_me_flag)
    login_failed = true unless user

    if login_failed
      flash[:error] = 'Login failed'
      redirect_to action: 'new'
    else
      user.process_latest_cart(@user_cart)
      flash[:notice] = 'Logged in successfully'
      redirect_with_saved_cookies front_root_path
    end
  end

  def destroy
    logout
    flash[:notice] = 'Logged out successfully'
    cookies.delete :cart_id
    redirect_with_saved_cookies front_root_path
  end

  private

  def session_params
    params.require(:user).permit!
  end
end
