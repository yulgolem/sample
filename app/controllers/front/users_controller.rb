class Front::UsersController < Front::BaseController

  before_action :check_user_profile, only: [:profile, :update_profile]
  skip_before_action :set_redirect_cookies, only: [:new, :create, :activate]

  skip_before_action :verify_authenticity_token, only: :cart

  def new
    @metadata = Page.find_by(target: 'sign_up') || @metadata
    if current_user
      redirect_with_saved_cookies front_root_path
    else
      @user = User.new
    end
  end

  def create
    @user = User.new(user_params)
    if @user.valid?
      @user.save
      @user_cart.update(user_id: @user.id, status: "active")
      flash[:notice] = "Thanks! Activation instructions have been sent to your email account!"
      redirect_with_saved_cookies front_root_path
    else
      render action: "new"
    end
  end

  def profile
    @user = current_user
    @user.valid?
  end

  def update_profile
    @user = User.find(current_user.id)
    @user.assign_attributes(user_params.except(:password, :password_confirmation))
    if @user.valid?
      @user.save
      redirect_to front_profile_path
    else
      render action: "profile"
    end
  end

  def activate
    if (@user = User.load_from_activation_token(params[:id]))
      @user.activate!
      auto_login(@user)
      redirect_with_saved_cookies front_profile_path, notice: 'User was successfully activated.'
    else
      redirect_to(front_root_path, error: "Something went wrong")
    end
  end

  def cart
    url = Rails.application.routes.recognize_path(request.referrer)
    @last_controller = url[:controller]
    @last_action = url[:action]

    @recently_registred = (@last_controller == 'front/users' && @last_action == 'new') ? true : false

    @price = @user_cart.order.total_price
    if @price > 0
      @paypal = PaypalInterface.new(@user_cart.order)
    end
  end

private

  def check_user_profile
    redirect_to front_root_path unless current_user
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name, :company, :country_code )
  end
end
