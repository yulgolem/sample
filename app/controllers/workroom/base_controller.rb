class Workroom::BaseController < ApplicationController
  layout "workroom_base"

  before_filter :check_orb_auth

  def check_orb_auth
    if !(current_user && current_user.role == 'analytic')
      redirect_to workroom_sign_in_url
    end
  end
end
