class Workroom::IndustriesController < Workroom::BaseController

  skip_before_action :verify_authenticity_token, only: :status
  skip_before_action :check_orb_auth, only: :status

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @update_date = Orb::Industry.first.updated_at.to_s(:default) unless Orb::Industry.first.blank?

    @industries = Orb::Industry.all

    @industries = @industries.paginate(page: page, per_page: per_page)
  end

  def refresh
    Orb::IndustryService.new.delay.get_data

    flash[:notice] = 'Industry processing is pending, it will end in no time.'

    redirect_to workroom_industries_path
  end

  def state
    @update_date = Orb::Industry.first.updated_at.to_s(:default) unless Orb::Industry.first.blank?
    result = Orb::Industry.any? { |industry| industry.status == 'pending' } ? {"status": "pending", "updated": @update_date} : {"status": "completed", "updated": @update_date}
    respond_to do |format|
      format.html { }
      format.js 
      format.json { render json: result } 
    end
  end
  

end
