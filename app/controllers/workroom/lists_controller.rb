class Workroom::ListsController < Workroom::BaseController
  require 'json'

  skip_before_action :verify_authenticity_token

  def new
    @naics_codes = []
    @sic_codes = []
    @technologies = []
    @tech_categories = []

    Orb::Naic.all.each do |naic|
      @naics_codes << naic.code.to_s + ' ' + naic.description
    end

    Orb::Sic.all.each do |sic|
      @sic_codes << sic.code.to_s + ' ' + sic.description
    end

    @list = Orb::List.new
  end

  def check_company_count
    query = {}
    clear_codes list_params
    list_params.each do |key, value|
      query[key.to_sym] = value unless key == 'title'
    end

    @list = Orb::List.new(title: list_params['title'], query_params: query.to_hash)
    count = {count: Orb::ListCompanyCountService.new(@list).get_count}
    respond_to do |format|
      format.html { }
      format.js
      format.json { render json: count }
    end
  end

  def create
    shift_params
    query = {}

    list_params.each do |key, value|
      query[key.to_sym] = value unless key == 'title'
    end

    @list = Orb::List.new(
      title: list_params['title'],
      query_params: query.to_json,
      importance_score: list_params['importance_score'],
      orb_num: list_params['orb_num'],
      entity_type: list_params['entity_type'],
      name: list_params['name'],
      name_prefix: list_params['name_prefix'],
      webdomain: list_params['webdomain'],
      industry: list_params['industry'],
      naics_codes: list_params['naics_codes'],
      sic_codes: list_params['sic_codes'],
      employees: list_params['employees'],
      revenue: list_params['revenue'],
      city: list_params['city'],
      state: list_params['state'],
      zip: list_params['zip'],
      country: list_params['country'],
      parent_orb_num: list_params['parent_orb_num'],
      rankings: list_params['rankings'],
      technologies: list_params['technologies'],
      tech_categories: list_params['tech_categories']
      )
    if @list.save
      Orb::ListService.new(@list).delay.get_list
      flash[:notice] = 'List processing is pending, as soon as list is ready it will appear in list index.'
      redirect_to workroom_lists_url
    else
      flash[:notice] = 'Error occured, please, try again.'
      redirect_to workroom_root_url
    end
  end

  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @lists = Orb::List.all.order('created_at DESC')
    @lists = @lists.paginate(page: page, per_page: per_page)
  end

  def download
    @list = Orb::List.find(params['id'].to_i)
    @list.update_attribute(:is_file_downloaded, true)
    respond_to do |format|
      format.csv { send_data @list.to_csv }
      format.xls { render layout: false }
    end
  end

  def state
    result = []
    list_data = Orb::List.last(10)
    list_data.each do |list|
      if list.status == 'completed'
        result << {"id": list.id, "status": 'completed', "count": list.result_count, "date": list.updated_at.to_s(:default)} 
      else
        result << {"id": list.id, "status": 'pending'}
      end
    end
    respond_to do |format|
      format.html { }
      format.js
      format.json { render json: result }
    end
  end

  private
  def list_params
    params.require(:orb_list).permit!
  end

  def shift_params
    list_params['naics_codes'].shift
    list_params['sic_codes'].shift
    list_params['employees'].shift
    list_params['revenue'].shift
    list_params['rankings'].shift
    list_params['technologies'].shift
    list_params['tech_categories'].shift
    
    clear_codes list_params
  end

  def clear_codes list_params
    unless list_params['naics_codes'].blank?
      naics_codes_refactored = []
      list_params['naics_codes'].each do |code|
        naics_codes_refactored << code.split.first
      end
      list_params['naics_codes'] = naics_codes_refactored.join(';')
    end

    unless list_params['sic_codes'].blank?
      sic_codes_refactored = []
      list_params['sic_codes'].each do |code|
        sic_codes_refactored << code.split.first
      end
      list_params['sic_codes'] = sic_codes_refactored.join(';')
    end

    list_params['employees'] = list_params['employees'].join(';') unless list_params['employees'].blank?
    list_params['revenue'] = list_params['revenue'].join(';') unless list_params['revenue'].blank?
    list_params['rankings'] = list_params['rankings'].join(';') unless list_params['rankings'].blank?
    list_params['technologies'] = list_params['technologies'].join(';') unless list_params['technologies'].blank?
    list_params['tech_categories'] = list_params['tech_categories'].join(';') unless list_params['tech_categories'].blank?
  end

  def attributes(list_params)
    {
      title: list_params['title'],
      orb_num: list_params['orb_num'],
      entity_type: list_params['entity_type'],
      name: list_params['name'],
      name_prefix: list_params['name_prefix'],
      webdomain: list_params['webdomain'],
      industry: list_params['industry'],
      naics_codes: list_params['naics_codes'],
      sic_codes: list_params['sic_codes'],
      employees: list_params['employees'],
      revenue: list_params['revenue'],
      city: list_params['city'],
      state: list_params['state'],
      zip: list_params['zip'],
      country: list_params['country'],
      parent_orb_num: list_params['parent_orb_num'],
      rankings: list_params['rankings'],
      technologies: list_params['technologies'],
      tech_categories: list_params['tech_categories'],
      importance_score: list_params['importance_score'],
      user: current_user,
      is_file_downloaded: false
    }
  end

end
