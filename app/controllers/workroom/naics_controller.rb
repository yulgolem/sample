class Workroom::NaicsController < Workroom::BaseController
  
  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @update_date = Orb::Naic.first.updated_at.to_s(:default) unless Orb::Naic.first.blank?

    @naics = Orb::Naic.all

    @naics = @naics.paginate(page: page, per_page: per_page)
  end

  def refresh
    Orb::NaicService.new.delay.get_data

    flash[:notice] = 'NAIC Codes processing is pending, it will end in no time.'

    redirect_to workroom_naics_path
  end

  def state
    @update_date = Orb::Naic.first.updated_at.to_s(:default) unless Orb::Naic.first.blank?
    result = Orb::Naic.any? { |naic| naic.status == 'pending' } ? {"status": "pending", "updated": @update_date} : {"status": "completed", "updated": @update_date}
    respond_to do |format|
      format.html { }
      format.js 
      format.json { render json: result } 
    end
  end
end
