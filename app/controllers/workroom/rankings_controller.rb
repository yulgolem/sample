class Workroom::RankingsController < Workroom::BaseController
  
  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @update_date = Orb::Ranking.first.updated_at.to_s(:default) unless Orb::Ranking.first.blank?

    @rankings = Orb::Ranking.all

    @rankings = @rankings.paginate(page: page, per_page: per_page)
  end

  def refresh
    Orb::RankingService.new.delay.get_data

    flash[:notice] = 'Ranking processing is pending, it will end in no time.'

    redirect_to workroom_rankings_path
  end

  def state
    @update_date = Orb::Ranking.first.updated_at.to_s(:default) unless Orb::Ranking.first.blank?
    result = Orb::Ranking.any? { |ranking| ranking.status == 'pending' } ? {"status": "pending", "updated": @update_date} : {"status": "completed", "updated": @update_date}
    respond_to do |format|
      format.html { }
      format.js 
      format.json { render json: result } 
    end
  end
end
