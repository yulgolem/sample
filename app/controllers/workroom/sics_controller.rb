class Workroom::SicsController < Workroom::BaseController
  
  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @update_date = Orb::Sic.first.updated_at.to_s(:default) unless Orb::Sic.first.blank?

    @sics = Orb::Sic.all

    @sics = @sics.paginate(page: page, per_page: per_page)
  end

  def refresh
    Orb::SicService.new.delay.get_data

    flash[:notice] = 'SIC Codes processing is pending, it will end in no time.'

    redirect_to workroom_sics_path
  end

  def state
    @update_date = Orb::Sic.first.updated_at.to_s(:default) unless Orb::Sic.first.blank?
    result = Orb::Sic.any? { |sic| sic.status == 'pending' } ? {"status": "pending", "updated": @update_date} : {"status": "completed", "updated": @update_date}
    respond_to do |format|
      format.html { }
      format.js 
      format.json { render json: result } 
    end
  end
end
