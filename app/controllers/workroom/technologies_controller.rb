class Workroom::TechnologiesController < Workroom::BaseController
  
  def index
    page = params[:page].try(:to_i) || 1
    per_page = params[:per_page].try(:to_i) || 10

    @update_date = Orb::Technology.first.updated_at.to_s(:default) unless Orb::Technology.first.blank?

    @technologies = Orb::Technology.all

    @technologies = @technologies.paginate(page: page, per_page: per_page)
  end

  def refresh
    Orb::TechnologyService.new.delay.get_data

    flash[:notice] = 'Technologies processing is pending, it will end in no time.'

    redirect_to workroom_technologies_path
  end

  def state
    @update_date = Orb::Technology.first.updated_at.to_s(:default) unless Orb::Technology.first.blank?
    result = Orb::Technology.any? { |technology| technology.status == 'pending' } ? {"status": "pending", "updated": @update_date} : {"status": "completed", "updated": @update_date}
    respond_to do |format|
      format.html { }
      format.js 
      format.json { render json: result } 
    end
  end
end
