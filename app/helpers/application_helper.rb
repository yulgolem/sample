module ApplicationHelper
  def resolve_alert_class_from_flash(type)
    case type
    when "error"
      "alert-danger"
    when "notice"
      "alert-success"
    end
  end

  def resolve_notfyjs_class(type)
    case type
    when "error"
      "error"
    when "notice"
      "success"
    end
  end

  def show_price(price)
    (price.to_i == price) ? "$#{price.to_i}" : "$#{price}"
  end

  def gtm_event(gtm_event)
    return gtm_event if defined?(gtm_event) && gtm_event.present?
    return 'gtm.js'
  end
end
