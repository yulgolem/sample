module WillPaginateHelper

  class PaginateRendererBootstrap < BootstrapPagination::Rails
  end

  def pagination_(collection, options = {})
    default_params = {:renderer => PaginateRendererBootstrap, inner_window: 1, outer_window: 1}
    will_paginate(collection, default_params.merge(options))
  end
end