class ContactMessagesMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.reset_password_email.subject
  #
  def contact_message_email(contact_message)
    @contact_message = contact_message
    mail(:to => GlobalSetting.get_instance.feedback_target_email,
       :subject => "Listoteca contact message from #{@contact_message.name}")
  end

end
