class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.reset_password_email.subject
  #
  def reset_password_email(user)
    @user = User.find user.id
    @url  = edit_password_reset_url(@user.reset_password_token)
    mail(:to => user.email,
       :subject => "Your password has been reset")
  end

  def activation_needed_email(user)
    @user = user
    @url  = activate_front_user_url(@user.activation_token)
    mail(:to => @user.email,
         :subject => "Welcome to Listoteca")
  end

  def activation_success_email(user)
    @user = user
    @url  = front_sign_in_url
    mail(:to => @user.email,
         :subject => "Your account on Listoteca is now activated")
  end

  def order_success_email(order, user)
    @user  = user
    @order = order
    mail(:to => @user.email,
          :subject => "Listoteca order #{@order.articulus}"

    )
  end

  


end
