class Cart < ApplicationRecord
  belongs_to :user
  has_many :cart_products, dependent: :destroy
  has_many :products, through: :cart_products, source: :product
  has_one :order, dependent: :destroy
end

# == Schema Information
#
# Table name: carts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  status     :string           default("active")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
