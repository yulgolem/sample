class CartProduct < ApplicationRecord
  belongs_to :cart
  belongs_to :product
end

# == Schema Information
#
# Table name: cart_products
#
#  id         :integer          not null, primary key
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  cart_id    :integer
#  price      :decimal(, )
#
