class Category < ApplicationRecord
  has_many :subcategories, :dependent => :destroy

  validates :title, presence: true, uniqueness: :true

  def allowed_to_destroy?
    !subcategories.joins(:product_subcategories).group("subcategories.id").having("product_subcategories.count > 0").any?
  end

  def kekekeke?
    Feature.all
  end
end

# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
