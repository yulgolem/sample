module ActsAsAdmin
  extend ActiveSupport::Concern

  included do
  end

  def is_admin?
    role == "admin"
  end

  def is_master_admin?
    role == "admin"
  end
    
end