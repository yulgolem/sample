module ProductVersionable
  extend ActiveSupport::Concern

  included do
    scope :base, -> {where(base_product_id: nil)}
    scope :latest_versions, ->{where(is_latest_version: true)}

    has_many :child_products, class_name: "Product", foreign_key: "base_product_id", dependent: :destroy
    belongs_to :base_product, class_name: "Product"

    after_create :make_latest_version
  end

  def select_last_version
    if self.child_products.present?
      return self.child_products.order("created_at ASC").last
    else
      return self
    end
  end

  def is_base_product?
    self.base_product_id == nil
  end

  def is_version_product?
    self.base_product_id != nil
  end

  def get_base_articulus
    is_base_product? ? articulus : base_product.articulus
  end

  def make_latest_version
    Product.where.not(base_product_id: nil, id: self.id).where(base_product_id: self.base_product_id).update_all(is_latest_version: false)
  end
    
end