class ContactMessage < ApplicationRecord
  validates :email, presence: true, format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates :name, presence: true, length: { minimum: 2,  maximum: 100 }
  validates :message, presence: true, length: { minimum: 2, maximum: 200 }
  belongs_to :user
end

# == Schema Information
#
# Table name: contact_messages
#
#  id         :integer          not null, primary key
#  email      :string
#  name       :string
#  message    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
