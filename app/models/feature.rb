class Feature < ApplicationRecord
  has_many :product_features, :dependent => :destroy
  has_many :products, through: :product_features, source: :product

  validates :title, presence: true
  validates :description, presence: true

  validates_with FeatureValidator

  has_attached_file :cover, styles: { medium: "40x40#" }, default_url: "/images/feature_:style_missing.png"
  validates_attachment_content_type :cover, content_type: /\Aimage\/.*\z/
  # validates :cover, attachment_presence: true

end

# == Schema Information
#
# Table name: features
#
#  id                 :integer          not null, primary key
#  title              :string
#  description        :string
#  cover_file_name    :string
#  cover_content_type :string
#  cover_file_size    :integer
#  cover_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  fa_icon_id         :string
#
