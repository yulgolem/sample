class GlobalSetting < ApplicationRecord
  validates :feedback_target_email, presence: true, format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  def self.get_instance
    instance = self.first
    instance ? instance : self.create(feedback_target_email: "info@listoteca.com")
  end
end

# == Schema Information
#
# Table name: global_settings
#
#  id                    :integer          not null, primary key
#  feedback_target_email :string
#
