class Orb::Company < ApplicationRecord
  belongs_to :orb_list, class_name: 'Orb::List'
end

# == Schema Information
#
# Table name: orb_companies
#
#  id                :integer          not null, primary key
#  orb_list_id       :integer
#  response          :json
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  rank              :string
#  name              :string
#  description       :string
#  website           :string
#  address1          :string
#  address2          :string
#  city              :string
#  state             :string
#  zip               :string
#  country           :string
#  revenue           :string
#  num_of_emp        :string
#  industry          :string
#  naics_code        :string
#  naics_description :string
#  year_founded      :string
#  phone             :string
#  fax               :string
#  email             :string
#
