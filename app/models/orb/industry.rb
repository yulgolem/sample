class Orb::Industry < ApplicationRecord
end

# == Schema Information
#
# Table name: orb_industries
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :string           default("pending")
#
