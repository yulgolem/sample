class Orb::List < ApplicationRecord
  require 'csv'
  require 'benchmark'
  
  belongs_to :user, class_name: 'User'
  has_many :orb_companies, class_name: 'Orb::Company', foreign_key: "orb_list_id"

  validates :title, presence: true

  has_attached_file :csv
  has_attached_file :xls

  do_not_validate_attachment_file_type :csv, :xls

  def generate_csv(options = {})
    desired_columns = ['rank','name','description','website','address1','address2','city','state','zip','country','revenue','num_of_emp','industry','naics_code','naics_description','year_founded','phone','fax','email']
    result = []

    @companies_hash = prepare_companies
    CSV.generate(options) do |csv|
      result = desired_columns
      @companies_hash.each do |company|
        result << company.values
      end
    end

    begin
      file = Tempfile.new(["orb_list_#{self.id}", '.csv'])
      file.write(result.join("\n"))
      self.csv = file
      save!
    ensure
      file.close unless file.nil?
    end
  end


  def generate_xls
    file     = File.open(Rails.root.to_s + '/app/views/workroom/lists/download.xls.erb', 'rb')
    template = file.read
    @companies_hash = prepare_companies
    locals   = { companies_hash: @companies_hash }

    result   = Erubis::Eruby.new(template).result(locals)
    begin
      file = Tempfile.new(["orb_list_#{self.id}", '.xls'])
      file.write(result)
      self.xls = file
      save!
    ensure
      file.close unless file.nil?
    end
  end

  def prepare_companies
    sql = "select rank,name,description,website,address1,address2,city,state,zip,country,revenue,num_of_emp,industry,\
           naics_code,naics_description,year_founded,phone,fax,email from orb_companies where orb_list_id = #{self.id}"
    companies_hash = ActiveRecord::Base.connection.execute(sql)
  end
end

# == Schema Information
#
# Table name: orb_lists
#
#  id                 :integer          not null, primary key
#  title              :string
#  orb_num            :integer
#  entity_type        :string
#  name               :string
#  name_prefix        :string
#  webdomain          :string
#  industry           :string
#  naics_codes        :string
#  sic_codes          :string
#  employees          :string
#  revenue            :string
#  city               :string
#  state              :string
#  zip                :string
#  country            :string
#  parent_orb_num     :integer
#  rankings           :string
#  technologies       :string
#  tech_categories    :string
#  importance_score   :string
#  user_id            :integer
#  query_params       :json
#  result_count       :integer
#  is_file_downloaded :boolean
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  status             :string           default("pending")
#  csv_file_name      :string
#  csv_content_type   :string
#  csv_file_size      :integer
#  csv_updated_at     :datetime
#  xls_file_name      :string
#  xls_content_type   :string
#  xls_file_size      :integer
#  xls_updated_at     :datetime
#  current_progress   :integer          default(0)
#
