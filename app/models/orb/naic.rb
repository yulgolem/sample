class Orb::Naic < ApplicationRecord
end

# == Schema Information
#
# Table name: orb_naics
#
#  id          :integer          not null, primary key
#  code        :integer
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  status      :string           default("pending")
#
