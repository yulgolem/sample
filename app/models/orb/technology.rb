class Orb::Technology < ApplicationRecord
end

# == Schema Information
#
# Table name: orb_technologies
#
#  id         :integer          not null, primary key
#  name       :string
#  category   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :string           default("pending")
#
