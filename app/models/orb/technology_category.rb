class Orb::TechnologyCategory < ApplicationRecord
end

# == Schema Information
#
# Table name: orb_technology_categories
#
#  id           :integer          not null, primary key
#  name         :string
#  technologies :text             is an Array
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  status       :string           default("pending")
#
