class Order < ApplicationRecord

  belongs_to :cart

  delegate :products, to: :cart

  before_create :generate_articulus
  after_save    :send_mail_after_payment

  scope :paid,    ->{where(status: "paid")}
  scope :pending, ->{where(status: "pending")}

  def generate_articulus
    options = [('A'..'Z'), (0..9)].map(&:to_a).flatten
    self.articulus = (0..5).map { options[rand(options.length)] }.join
  end

  def paid?
    self.status == "paid"
  end

  def pending?
    self.status == "pending"
  end


  def send_mail_after_payment
    if status_changed? && paid?
      UserMailer.order_success_email(self, self.cart.user).deliver
    end
  end

  def self.search(string)
    if string
      joins('join carts on orders.cart_id = carts.id join users on carts.user_id = users.id').
        where('orders.articulus like ? OR users.email like ?', "%#{string}%", "%#{string}%")
    else
      joins('join carts on orders.cart_id = carts.id join users on carts.user_id = users.id')
    end
  end
end

# == Schema Information
#
# Table name: orders
#
#  id             :integer          not null, primary key
#  cart_id        :integer
#  status         :string           default("pending")
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  articulus      :string
#  payment_token  :string
#  total_price    :decimal(, )
#  transaction_id :string
#
