class Page < ApplicationRecord
  validates :target, presence: true, uniqueness: true
end

# == Schema Information
#
# Table name: pages
#
#  id            :integer          not null, primary key
#  target        :string
#  meta_title    :string
#  meta_desc     :string
#  meta_keywords :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
