class Product < ApplicationRecord

  include ActiveModel::Validations
  include ActiveModel::Dirty
  include ProductVersionable

  scope :filter_category, -> (product_ids) {[product_ids].flatten!; joins(:subcategories).where(subcategories: {id: product_ids})}
  scope :filter_price, -> (price_from, price_to) {where('price between ? and ?', price_from, price_to)}
  scope :unhidden, -> {where("is_hidden IS NOT TRUE")}

  has_many :product_subcategories, dependent: :destroy
  has_many :subcategories, through: :product_subcategories, source: :subcategory
  has_many :product_features, dependent: :destroy
  has_many :features, through: :product_features, source: :feature
  has_many :cart_products, dependent: :destroy

  before_create :generate_articulus
  before_create :set_dates_to_now

  after_validation :assign_attachment_file_names, on: :update

  validates :title, presence: true
  validates :description, presence: true
  validates :detailed_description, presence: true
  validates :price, presence: true, numericality: {greater_than_or_equal_to: 0.0, less_than: 1000000.0}
  validates :list_length, presence: true, numericality: {greater_than_or_equal_to: 0, less_than: 100000000.0}
  validates :last_updated_at_pub, presence: true
  validates :last_updated_at_internal, presence: true


  # validates :discount, numericality: {less_than_or_equal_to: 100.0, greater_than_or_equal_to: 0, allow_nil: true}
  validates_with ProductValidator

  has_attached_file :cover, styles: { medium: "300x300#" }, default_url: "/images/product_:style_missing.png"
  # validates_attachment_content_type :cover, content_type: /\Aimage\/.*\z/
  # validates :cover, attachment_presence: true

  has_attached_file :txt_list
  validates_attachment_content_type :txt_list, content_type: ["text/plain", "text/html"]
  #validates_format_of :txt_list_file_name, :with => %r{\.(txt)\z}
  #validates :txt_list, attachment_presence: true

  has_attached_file :xls_list
  validates_attachment_content_type :xls_list, content_type: ["application/x-ole-storage", "application/vnd.ms-excel", "application/xml", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]
  #validates_format_of :xls_list_file_name, :with => %r{\.(xls|xlsx)\z}i
  #validates :xls_list, attachment_presence: true

  has_attached_file :csv_list
  validates_attachment_content_type :csv_list, content_type: "text/plain"
  #validates_format_of :csv_list_file_name, :with => %r{\.(csv)\z}i, allow_nil: true
  #validates_attachment_content_type :csv_list, content_type: "text/plain"
  #validates :csv_list, attachment_presence: true

  has_attached_file :example_list
  validates_attachment_content_type :csv_list, content_type: ["text/plain", "text/html", "application/x-ole-storage", "application/vnd.ms-excel", "application/xml", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]
  #validates_format_of :example_list_file_name, :with => %r{\.(xls|txt|csv|xlsx)\z}i, allow_nil: true
  #validates :example_list, attachment_presence: true

  def public_cover(style = nil)
    cover.present? ? cover.url(style) : '/images/ls_product_icon.png'
  end

  def versionize_with(params)
    product_to_return = Product.new(params.merge(base_product_id: self.id))
    if !product_to_return.cover.present?
      product_to_return.cover = self.cover
    end
    return product_to_return
  end

  def assign_attachment_file_names
    if self.txt_list_updated_at_changed?
      extension = File.extname(txt_list_file_name).downcase
      self.txt_list.instance_write(:file_name, "#{articulus}_#{Time.now.to_i.to_s}#{extension}")
    end

    if self.xls_list_updated_at_changed?
      extension = File.extname(xls_list_file_name).downcase
      self.xls_list.instance_write(:file_name, "#{articulus}_#{Time.now.to_i.to_s}#{extension}")
    end
    if self.csv_list_updated_at_changed?
      extension = File.extname(csv_list_file_name).downcase
      self.csv_list.instance_write(:file_name, "#{articulus}_#{Time.now.to_i.to_s}#{extension}")
    end
    if self.example_list_updated_at_changed?
      extension = File.extname(example_list_file_name).downcase
      self.example_list.instance_write(:file_name, "#{articulus}_#{Time.now.to_i.to_s}_example#{extension}")
    end
  end

  def show_public_price
    (price.to_i == price) ? "$#{price.to_i}" : "$#{price}"
  end

  def show_discounted_price
    (price_discounted.to_i == price_discounted) ? "$#{price_discounted.to_i}" : "$#{price_discounted}"
  end

  def show_public_discount
    discount
  end

  def hide!
    self.update_attributes(is_hidden: true)
  end

  def show!
    self.update_attributes(is_hidden: false)
  end

  def generate_articulus
    options = [('A'..'Z'), (0..9)].map(&:to_a).flatten
    self.articulus = (0..5).map { options[rand(options.length)] }.join
    self.assign_attachment_file_names()
  end

  def set_dates_to_now
    self.last_updated_at_pub       = Date.today
    self.last_updated_at_internal  = Date.today
  end

  def downloads_by_user(user_id)
    product_downloads = ProductDownload.where(user_id: user_id).where(product_id: id)
  end

end

# == Schema Information
#
# Table name: products
#
#  id                        :integer          not null, primary key
#  title                     :string
#  articulus                 :string
#  base_product_id           :integer
#  cover_file_name           :string
#  cover_content_type        :string
#  cover_file_size           :integer
#  cover_updated_at          :datetime
#  txt_list_file_name        :string
#  txt_list_content_type     :string
#  txt_list_file_size        :integer
#  txt_list_updated_at       :datetime
#  xls_list_file_name        :string
#  xls_list_content_type     :string
#  xls_list_file_size        :integer
#  xls_list_updated_at       :datetime
#  csv_list_file_name        :string
#  csv_list_content_type     :string
#  csv_list_file_size        :integer
#  csv_list_updated_at       :datetime
#  example_list_file_name    :string
#  example_list_content_type :string
#  example_list_file_size    :integer
#  example_list_updated_at   :datetime
#  last_updated_at_pub       :datetime
#  last_updated_at_internal  :datetime
#  description               :text
#  detailed_description      :text
#  list_length               :integer
#  price                     :decimal(, )
#  discount                  :string
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  list_length_commentary    :text
#  is_latest_version         :boolean          default(TRUE)
#  structure                 :text
#  price_discounted          :decimal(, )
#  is_hidden                 :boolean          default(FALSE)
#  label_bg_color            :string           default("#e27c7c")
#  meta_title                :string
#  meta_desc                 :string
#  meta_keywords             :string
#
