class ProductDownload < ApplicationRecord
  has_one :product
  has_one :user

  def add_csv
    self.file_downloaded_format = 'csv'
    self.save!
  end

  def add_xls
    self.file_downloaded_format = 'xls'
    self.save!
  end

  def add_txt
    self.file_downloaded_format = 'txt'
    self.save!
  end
end
