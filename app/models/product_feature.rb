class ProductFeature < ApplicationRecord
  belongs_to :product
  belongs_to :feature
end

# == Schema Information
#
# Table name: product_features
#
#  id         :integer          not null, primary key
#  product_id :integer
#  feature_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
