class ProductSubcategory < ApplicationRecord
  belongs_to :product
  belongs_to :subcategory
end

# == Schema Information
#
# Table name: product_subcategories
#
#  id             :integer          not null, primary key
#  product_id     :integer
#  subcategory_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
