class Subcategory < ApplicationRecord
  belongs_to :category
  has_many :product_subcategories, :dependent => :destroy
  has_many :products, through: :product_subcategories, source: :product

  validates :title, presence: true, uniqueness: true

  def allowed_to_destroy?
    !product_subcategories.any?
  end
end

# == Schema Information
#
# Table name: subcategories
#
#  id          :integer          not null, primary key
#  title       :string
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
