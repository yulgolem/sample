class User < ApplicationRecord
  authenticates_with_sorcery!

  include ActiveModel::Validations
  include ActiveModel::Dirty
  include ActsAsAdmin

  attr_accessor :remember
  attr_accessor :skip_activation_email
  attr_accessor :skip_activation_success_email

  authenticates_with_sorcery! do |config|
    config.authentications_class = Authentication
  end

  has_many :authentications, dependent: :destroy
  accepts_nested_attributes_for :authentications
  has_many :carts, dependent: :destroy
  has_many :orders, through: :carts, source: :order
  has_many :lists, class_name: 'Orb::List'

  before_update :send_confirmation_email!

  validates :email, presence: true, uniqueness: true, format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates :first_name, presence: true, length: { minimum: 2,  maximum: 100 }, if: -> { role == "plane" }
  validates :last_name, presence: true, length: { minimum: 2, maximum: 100 }, if: -> { role == "plane" }
  validates :company, presence: true, length: { minimum: 2, maximum: 200 }, if: -> { role == "plane" }
  validates :country_code, presence: true, if: -> {  }

  validates :password, length: { minimum: 3 }, if: -> { new_record? || changes[:crypted_password] }
  validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] }

  validates_with UserValidator

  def cart
    carts.where(status: "active").order("created_at ASC").last
  end

  def bought_product?(product_id)
    return true if role == 'admin'
    
    orders.paid.each do |order|
      return true if order.products.pluck(:id).include?(product_id)
    end
    return false
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def country_name
    country = ISO3166::Country[country_code]
    if country
      return country.translations[I18n.locale.to_s] || country.name
    else
      return ""
    end
  end

  def activate!
    super
    self.update(is_email_confirmed: true)
  end

  def send_confirmation_email!
    if email_changed? && !skip_activation_email
      setup_activation
      send_activation_needed_email!
    end
  end

  def send_activation_needed_email!
    super unless skip_activation_email
  end

  def send_activation_success_email!
    super unless skip_activation_success_email
  end

  def process_latest_cart(attach_cart)
    if cart && cart.cart_products.count > 0
      return nil
    else
      Cart.where(user_id: self.id, status: "active").update_all(status: "inactive") unless (cart && cart.id == attach_cart.id)
      attach_cart.update(user_id: self.id, status: "active")
    end
  end

end

# == Schema Information
#
# Table name: users
#
#  id                              :integer          not null, primary key
#  email                           :string
#  crypted_password                :string
#  salt                            :string
#  is_admin                        :boolean          default(FALSE)
#  is_email_confirmed              :boolean          default(FALSE)
#  secret_token                    :string
#  remember_me_token               :string
#  remember_me_token_expires_at    :datetime
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  reset_password_token            :string
#  reset_password_token_expires_at :datetime
#  reset_password_email_sent_at    :datetime
#  role                            :string           default("user")
#  first_name                      :string
#  last_name                       :string
#  company                         :string
#  country_code                    :string
#  activation_state                :string
#  activation_token                :string
#  activation_token_expires_at     :datetime
#
