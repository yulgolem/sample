class FeatureValidator < ActiveModel::Validator

  def validate(record)
    if record.fa_icon_id.blank? && record.cover.blank?
      record.errors[:fa_icon_id] << "can't be blank if cover is blank"
      record.errors[:cover] << "can't be blank if font awesome id is blank"
    end
  end

end