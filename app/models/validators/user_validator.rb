class UserValidator < ActiveModel::Validator

  def validate(record)
    if record.email_changed? && !record.email_was.blank?
      record.errors[:email] << "Sorry. You can't edit email"
    end
  end

end