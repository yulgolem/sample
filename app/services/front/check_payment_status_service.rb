module Front
  class CheckPaymentStatusService
    require 'paypal-sdk-rest'

    attr_accessor :order

    def initialize(order)
      @order = order
    end

    def execute
      case @order.payment_method
      when 'stripe'
        unless @order.created_at > Date.new(2017, 5, 06)
          charge = Stripe::Charge.retrieve(
            @order.transaction_id
          )
          return charge.paid
        else
          return true
        end
      when 'paypal'
        payment = PayPal::SDK::REST::Payment.find(@order.payment_token)
        return true if payment.state == 'approved'
      else
        return false
      end
      return false
    rescue
      return false
    end

  end
end