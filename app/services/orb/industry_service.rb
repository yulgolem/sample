module Orb
  class IndustryService < BaseService
    
    def initialize
    end

    def get_data
      Orb::Industry.update_all(status: 'pending')
      result = RestClient.get("#{API_HOST}/dictionaries/industries?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |industry|
        orb_industry = Orb::Industry.find_or_create_by(name: industry['name'])
        orb_industry.update_attribute(:status, 'completed')
      end
    rescue
      p $!
    end

  end
end