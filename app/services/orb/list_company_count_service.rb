module Orb
  class ListCompanyCountService < ListService

    def get_count
      url_string = @list.query_params.to_query
      json = get_data(url_string)
      total_count = json['result_count']
    rescue Exception
      return false
    end

  end
end