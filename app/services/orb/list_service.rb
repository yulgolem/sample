module Orb
  class ListService < BaseService

    attr_accessor :list

    def initialize list
      @list = list
    end

    def get_list
      url_string = JSON.parse(@list.query_params).to_query
      json = get_data(url_string)
      total_count = json['result_count']
      if total_count <= 100
        parse_result_set(json['result_set'])
      else
        end_of_data = (json['result_count']/100)+1
        for i in 0..end_of_data
          parse_result_set(get_data(url_string, i*100)['result_set'])
          current_progress = (i*100/end_of_data)
          @list.update_attribute(:current_progress, current_progress)
        end
      end
      @list.generate_xls
      @list.generate_csv
      @list.update_attributes(result_count: total_count, status: 'completed')
      return true
    rescue Exception
      return false
    end

    private

    def get_data(url_string, offset = 0)
      url = "#{API_HOST}/search/companies?api_key=#{API_KEY}&limit=100&offset=#{offset}#{url_string}"
      result = RestClient.get(url)
      json = JSON.parse(result.body)
    end

    def parse_result_set(result_set)
      result_set.each do |company|
        attributes = {
          orb_list_id: @list.id,
          rank: company['search_rank'],
          name: company['name'],
          description: company['description'],
          website: company['website'],
          address1: company['address']['address1'],
          address2: company['address']['address2'],
          city: company['address']['city'],
          state: company['address']['state'],
          zip: company['address']['zip'],
          country: company['address']['country'],
          revenue: company['revenue'],
          num_of_emp: company['employees'],
          industry: company['industry'],
          naics_code: company['naics_code'],
          naics_description: company['naics_description'],
          year_founded: company['year_founded'],
          phone: company['phone'],
          fax: company['fax'],
          email: company['email']
        }
        company = Orb::Company.new(attributes)
        company.save
      end
    end

  end
end
