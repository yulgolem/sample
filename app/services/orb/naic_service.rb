module Orb
  class NaicService < BaseService
    
    def initialize
    end

    def get_data
      Orb::Naic.update_all(status: 'pending')
      result = RestClient.get("#{API_HOST}/dictionaries/naics_codes?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |naic|
        orb_naic = Orb::Naic.find_or_create_by(code: naic['code'].to_i)
        orb_naic.update_attribute(:description, naic['description'])
      end
    rescue
      p $!
    end

  end
end