module Orb
  class RankingService < BaseService
    
    def initialize
    end

    def get_data
      Orb::Ranking.update_all(status: 'pending')
      result = RestClient.get("#{API_HOST}/dictionaries/rankings?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |ranking|
        orb_ranking = Orb::Ranking.find_or_create_by(name: ranking['name'])
      end
    rescue
      p $!
    end

  end
end