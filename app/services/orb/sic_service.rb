module Orb
  class SicService < BaseService
    
    def initialize
    end

    def get_data
      Orb::Sic.update_all(status: 'pending')
      result = RestClient.get("#{API_HOST}/dictionaries/sic_codes?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |sic|
        orb_sic = Orb::Sic.find_or_create_by(code: sic['code'].to_i)
        orb_sic.update_attribute(:description, sic['description'])
      end
    rescue
      p $!
    end

  end
end