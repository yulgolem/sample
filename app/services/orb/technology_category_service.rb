module Orb
  class TechnologyCategoryService < BaseService
    
    def initialize
    end

    def get_data
      Orb::TechnologyCategory.update_all(status: 'pending')
      result = RestClient.get("#{API_HOST}/dictionaries/technologies/categories?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |category|
        orb_technology_category = Orb::TechnologyCategory.find_or_create_by(name: category['name'])
        orb_technology_category.update_attribute(:technologies, category['technologies'])
      end
    rescue
      p $!
    end

  end
end