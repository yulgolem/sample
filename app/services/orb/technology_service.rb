module Orb
  class TechnologyService < BaseService
    
    def initialize
    end

    def get_data
      Orb::Technology.update_all(status: 'pending')
      result = RestClient.get("#{API_HOST}/dictionaries/technologies?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |technology|
        orb_technology = Orb::Technology.find_or_create_by(name: technology['name'])
        orb_technology.update_attribute(:category, technology['category'])
      end
    rescue
      p $!
    end

  end
end