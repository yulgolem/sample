require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Listoteca
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    #PayPal::SDK::Core::Config.load('config/paypal.yml',  ENV['RACK_ENV'] || 'development')
    #PayPal::SDK.logger = Logger.new(STDERR)
    config.action_dispatch.default_headers.merge!({
      'Access-Control-Allow-Origin' => '*',
      'Access-Control-Request-Method' => '*'
    })
    Paperclip.options[:content_type_mappings] = {:csv => "text/plain"}
    config.enable_dependency_loading = true
    config.autoload_paths << Rails.root.join('app/models/validators')
    config.autoload_paths << Rails.root.join('app/services')
    config.autoload_paths << Rails.root.join('lib')
    config.exceptions_app = self.routes
    default_url_options[:host] = Rails.application.secrets.email_host
    config.action_mailer.perform_deliveries = true
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
        address:              Rails.application.secrets.smtp_address,
        port:                 587,
        user_name:            Rails.application.secrets.email_username,
        password:             Rails.application.secrets.email_password,
        authentication:       'plain',
        enable_starttls_auto: true
    }
  end
end
