require 'capistrano-db-tasks'

set :application,            'listoteca'

# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

set :scm,                    'git'
set :repo_url,               'git@gitlab.com:Dsh/listoteca.com.git'
set :additional_stages,      %w(production)
set :more_shared_settings,   %w(database.yml)
set :use_sudo,               false
set :rvm_type,               'system'

set :keep_releases, 2

set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :delayed_job do 

  desc "Start the delayed_job process"
  task :start do
    on roles(:all) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :'bin/delayed_job', :"start"
        end
      end
    end
  end

  desc "Stop the delayed_job process"
  task :stop do
    on roles(:all) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :'bin/delayed_job', :"stop"
        end
      end
    end
  end

  desc "Restart the delayed_job process"
  task :restart do
    invoke 'delayed_job:stop'
    invoke 'delayed_job:start'
  end

end

after 'deploy:publishing', 'delayed_job:restart'