# -*- encoding : utf-8 -*-
set :user, 'webdev'

server 'listoteca.com', user: 'webdev', roles: %w{web app db}
set :use_sudo, false
set :branch, :staging

set :deploy_to,   "/var/www/staging.listoteca.com"
set :passenger_restart_with_touch, true
set :passenger_rvm_ruby_version, '2.3'

set :linked_files,     %w{config/database.yml}
