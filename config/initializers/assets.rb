# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile += %w( admin_base.css admin_base.js front_base.css front_base.js workroom_base.js )
Rails.application.config.assets.precompile += %w( workroom/industries/status.js )
Rails.application.config.assets.precompile += %w( workroom/naics/status.js )
Rails.application.config.assets.precompile += %w( workroom/sics/status.js )
Rails.application.config.assets.precompile += %w( workroom/technologies/status.js )
Rails.application.config.assets.precompile += %w( workroom/technology_categories/status.js )
Rails.application.config.assets.precompile += %w( workroom/lists/status.js workroom/lists/count.js )

