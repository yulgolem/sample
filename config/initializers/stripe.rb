
if ['production'].include?(Rails.env)
  Rails.configuration.stripe = {
    :publishable_key => '',
    :secret_key      => ''
  }
else
  Rails.configuration.stripe = {
    :publishable_key => '',
    :secret_key      => ''
  }
end
Stripe.api_key = Rails.configuration.stripe[:secret_key]
