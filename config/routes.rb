Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: "front/pages#welcome"

  namespace :workroom, path: "workroom" do
    root to: 'lists#new'
    get "sign_in" => "sessions#new"

    resources :lists do
      member do
        get :download
      end
      collection do
        get :state
        get :check_company_count
      end
    end

    resources :industries, only: [:index] do
      collection do
        post :refresh
        get :state
      end
    end

    resources :naics, only: [:index] do
      collection do
        post :refresh
        get :state
      end
    end

    resources :sics, only: [:index] do
      collection do
        post :refresh
        get :state
      end
    end

    resources :rankings, only: [:index] do
      collection do
        post :refresh
        get :state
      end
    end

    resources :sessions, only: [:create] do
      collection do
        delete :destroy
      end
    end

    resources :technologies, only: [:index] do
      collection do
        post :refresh
        get :state
      end
    end

    resources :technology_categories, only: [:index] do
      collection do
        post :refresh
        get :state
      end
    end
  end



  namespace :admin, path: "admin" do
    root to: "products#index"
    get "sign_in" => "sessions#new"

    resources :clients do
      member do
        post :activate
      end
    end
    resources :products do
      member do
        get :new_version
        get :download_list
        put :upload_attachment
        post :create_version
        post :hide
        post :unhide
      end
    end
    resources :pages do
    end
    resources :orders, param: :articulus do
      collection do
        get :get_data
      end
    end
    resources :features do
    end
    resources :administrators do
    end
    resources :categories do
    end
    resources :subcategories do
    end

    resources :settings do
    end
    resources :contact_messages, path: "feedback" do
      collection do
        post :update_feedback_email
      end
    end
    resources :sessions, only: [:create] do
      collection do
        delete :destroy
      end
    end
  end

  namespace :front, path: "/" do
    root to: "pages#welcome"

    match "/404", :to => "errors#not_found", :via => :all

    get "sign_in" => "sessions#new"
    get "sign_up" => "users#new"

    get  "/orders"   => "orders#index"
    get  "/approved" => "orders#approved"
    post "/approved" => "orders#approved"
    post "/pay" => "orders#pay"

    get   "profile" => "users#profile"
    patch "profile" => "users#update_profile"

    get "oauth/callback"  => "oauths#callback"
    get "oauth/:provider" => "oauths#oauth", :as => :auth_at_provider

    get "cart"     => "users#cart"
    get "checkout" => "base#checkout"

    resources :orders, param: :articulus do
      collection do
        post :stripe_success
        get  :paypal_success
      end
    end
    resources :contact_messages, only: [:create]
    resources :pages, path: "/", only: [] do
      collection do
        get :faq
        get :about_us, path: "aboutus"
        get :privacy
        get :terms_of_use
      end
    end
    resources :products, path: "readylists", only: [:index, :show] do
      collection do
        get :list
        get :subcategory
      end
      member do
        get    :download_example
        get    :download_list
        # TODO: Cart controller
        post   :add_to_cart
        delete :remove_from_cart
      end
    end
    resources :users do
      member do
        get :activate
      end
    end
    resources :sessions, only: [:create] do
      collection do
        delete :destroy
      end
    end
  end

  resources :password_resets
end
