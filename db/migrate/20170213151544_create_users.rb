class CreateUsers < ActiveRecord::Migration[5.0]
  def self.up
    create_table :users do |t|
      t.string   :full_name
      t.string   :email
      t.string   :crypted_password
      t.string   :salt
      t.boolean  :is_admin, default: false
      t.boolean  :is_email_confirmed, default: false
      t.string   :secret_token
      t.string   :remember_me_token
      t.datetime :remember_me_token_expires_at

      t.timestamps
    end
  end

  def self.down
    drop_table :users
  end
end
