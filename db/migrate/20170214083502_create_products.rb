class CreateProducts < ActiveRecord::Migration[5.0]
  def self.up
    create_table :products do |t|
      t.string :title
      t.string :articulus
      t.boolean :is_base
      t.integer :base_product_id
      t.attachment :cover
      t.attachment :txt_list
      t.attachment :xls_list
      t.attachment :csv_list
      t.attachment :example_list
      t.datetime :last_updated_at_pub
      t.datetime :last_updated_at_internal
      t.text :description
      t.text :detailed_description
      t.integer :list_length
      t.integer :list_length_commentary
      t.string :data_structure
      t.decimal :price
      t.decimal :discount
      t.string :status

      t.timestamps
    end
  end
end
