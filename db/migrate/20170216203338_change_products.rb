class ChangeProducts < ActiveRecord::Migration[5.0]
  def self.up
    remove_column :products, :is_base
    remove_column :products, :list_length_commentary
    remove_column :products, :data_structure
    remove_column :products, :status

    add_column :products,  :list_length_commentary, :text

  end

  def self.down
    remove_column :products, :list_length_commentary

    add_column :products, :is_base,                :boolean
    add_column :products, :list_length_commentary, :integer
    add_column :products, :data_structure,         :string
    add_column :products, :status,                 :string
  end
end
