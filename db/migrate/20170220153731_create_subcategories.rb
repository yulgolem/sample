class CreateSubcategories < ActiveRecord::Migration[5.0]
  def change
    create_table :subcategories do |t|
      t.string :title
      t.integer :category_id

      t.timestamps
    end

    add_index :subcategories, :category_id
  end
end
