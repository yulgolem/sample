class CreateProductSubcategories < ActiveRecord::Migration[5.0]
  def change
    create_table :product_subcategories do |t|
      t.integer :product_id
      t.integer :subcategory_id

      t.timestamps
    end

    add_index :product_subcategories, :product_id
    add_index :product_subcategories, :subcategory_id
    add_index :product_subcategories, [:product_id, :subcategory_id], unique: true
  end
end
