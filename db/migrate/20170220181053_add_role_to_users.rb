class AddRoleToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :role, :string, default: "plane"

    User.where(is_admin: true).update_all(role: "master")
  end
end
