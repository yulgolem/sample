class ChangeUserNameFields < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :full_name, :string
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :company, :string
    add_column :users, :country_code, :string
  end
end
