class AddIsLatestVersionToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :is_latest_version, :boolean, default: true
  end
end
