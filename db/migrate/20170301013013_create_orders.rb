class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :articulus
      t.integer :user_id
      t.integer :product_id
      t.integer :count
      t.string :status
      
      t.timestamps
    end
  end
end
