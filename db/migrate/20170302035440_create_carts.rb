class CreateCarts < ActiveRecord::Migration[5.0]
  def change
    create_table :carts do |t|
      t.integer :user_id
      t.string  :status

      t.timestamps
    end

    remove_column :orders, :user_id, :integer
    remove_column :orders, :count,   :integer
    add_column    :orders, :cart_id, :integer
    add_column    :orders, :price,   :decimal
  end
end
