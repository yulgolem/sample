class ProductFeatures < ActiveRecord::Migration[5.0]
  def change
    create_table :product_features do |t|
      t.integer :product_id
      t.integer :feature_id

      t.timestamps
    end

    add_index :product_features, :product_id
    add_index :product_features, :feature_id
    add_index :product_features, [:product_id, :feature_id], unique: true
  end
end
