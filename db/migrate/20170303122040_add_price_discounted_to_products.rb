class AddPriceDiscountedToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :price_discounted, :decimal
  end
end
