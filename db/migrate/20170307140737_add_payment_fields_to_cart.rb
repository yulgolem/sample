class AddPaymentFieldsToCart < ActiveRecord::Migration[5.0]
  def change
    add_column :carts, :payment_token, :string
    add_column :carts, :total_price, :decimal
  end
end
