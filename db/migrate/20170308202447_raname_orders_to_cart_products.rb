class RanameOrdersToCartProducts < ActiveRecord::Migration[5.0]
  def change
    rename_table :orders, :cart_products
  end
end
