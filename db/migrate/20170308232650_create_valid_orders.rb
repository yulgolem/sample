class CreateValidOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.integer :cart_id
      t.string  :status, default: "pending"

      # other fields

      t.timestamps
    end

    remove_column :cart_products, :articulus, :string
    remove_column :cart_products, :status, :string
  end
end
