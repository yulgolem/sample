class AddDefaultActiveToCartsStatus < ActiveRecord::Migration[5.0]
  def change
    change_column :carts, :status, :string, default: "active"
  end
end
