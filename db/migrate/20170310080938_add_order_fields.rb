class AddOrderFields < ActiveRecord::Migration[5.0]
  def change
    remove_column :carts, :payment_token, :string
    remove_column :carts, :total_price, :decimal

    add_column :orders, :payment_token, :string
    add_column :orders, :total_price, :decimal
    add_column :orders, :transaction_id, :string
  end
end
