class ChangeDiscountColumn < ActiveRecord::Migration[5.0]
  def change
    change_column :products, :discount, :string
  end
end
