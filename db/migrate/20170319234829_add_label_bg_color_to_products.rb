class AddLabelBgColorToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :label_bg_color, :string, default: "#e27c7c"
  end
end
