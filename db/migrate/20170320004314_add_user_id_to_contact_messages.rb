class AddUserIdToContactMessages < ActiveRecord::Migration[5.0]
  def change
    add_column :contact_messages, :user_id, :integer
  end
end
