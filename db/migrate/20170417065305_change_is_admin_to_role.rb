class ChangeIsAdminToRole < ActiveRecord::Migration[5.0]
  def up
    change_column :users, :role, :string, default: "user"
    User.where(is_admin: true).update_all(role: "admin")
    User.where(is_admin: false).update_all(role: "user")
  end

  def down
    change_column :users, :role, :string, default: "plane"
    User.where(role: "admin").update_all(is_admin: true)
    User.where(role: "user").update_all(is_admin: false)
    User.where(is_admin: true).update_all(role: "master")
    User.where(is_admin: false).update_all(role: "plane")
  end
end
