class CreateOrbNaics < ActiveRecord::Migration[5.0]
  def change
    create_table :orb_naics do |t|
      t.integer :code
      t.string :description
      
      t.timestamps
    end
  end
end
