class CreateOrbTechnologies < ActiveRecord::Migration[5.0]
  def change
    create_table :orb_technologies do |t|
      t.string :name
      t.string :category

      t.timestamps
    end
  end
end
