class CreateOrbTechnologyCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :orb_technology_categories do |t|
      t.string :name
      t.text :technologies, array: true

      t.timestamps
    end
  end
end
