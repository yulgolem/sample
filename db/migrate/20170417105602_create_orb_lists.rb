class CreateOrbLists < ActiveRecord::Migration[5.0]
  def change
    create_table :orb_lists do |t|
      t.string :title
      t.integer :orb_num
      t.string :entity_type
      t.string :name
      t.string :name_prefix
      t.string :webdomain
      t.string :industry
      t.string :naics_codes
      t.string :sic_codes
      t.string :employees
      t.string :revenue
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.integer :parent_orb_num
      t.string :rankings
      t.string :technologies
      t.string :tech_categories
      t.string :importance_score
      t.belongs_to :user
      t.json :result
      t.integer :result_count
      t.boolean :is_file_downloaded


      t.timestamps
    end
  end
end
