class CreateBlobalSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :global_settings do |t|
      t.string :feedback_target_email
    end

    GlobalSetting.create(feedback_target_email: "info@listoteca.com")
  end
end
