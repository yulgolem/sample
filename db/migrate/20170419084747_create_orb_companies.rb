class CreateOrbCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :orb_companies do |t|
      t.belongs_to :orb_list
      t.json :response

      t.timestamps
    end
  end
end
