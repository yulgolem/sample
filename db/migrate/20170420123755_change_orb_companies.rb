class ChangeOrbCompanies < ActiveRecord::Migration[5.0]
  def change
    change_table :orb_companies  do |t|
      t.string :rank
      t.string :name
      t.string :description
      t.string :website
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.string :revenue
      t.string :num_of_emp
      t.string :industry
      t.string :naics_code
      t.string :naics_description
      t.string :year_founded
      t.string :phone
      t.string :fax
      t.string :email
    end
  end
end
