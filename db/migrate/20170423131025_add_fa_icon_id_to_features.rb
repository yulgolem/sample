class AddFaIconIdToFeatures < ActiveRecord::Migration[5.0]
  def change
    add_column :features, :fa_icon_id, :string
  end
end
