class CreatePages < ActiveRecord::Migration[5.0]
  def change
    create_table :pages do |t|
      t.string :target
      t.string :meta_title
      t.string :meta_desc
      t.string :meta_keywords

      t.timestamps
    end


    %w(welcome terms_of_use aboutus privacy sign_up sign_in reset_password readylists faq).each do |target|
      Page.create(target: target)
    end
  end
end
