class AddMetadataFieldsToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :meta_title, :string
    add_column :products, :meta_desc, :string
    add_column :products, :meta_keywords, :string
  end
end
