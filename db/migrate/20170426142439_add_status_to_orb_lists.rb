class AddStatusToOrbLists < ActiveRecord::Migration[5.0]
  def change
    change_table :orb_lists do |t|
      t.string :status, default: 'pending'
    end
  end
end
