class AddStatusToOrb < ActiveRecord::Migration[5.0]
  def change
    change_table :orb_naics do |t|
      t.string :status, default: 'pending'
    end

    change_table :orb_sics do |t|
      t.string :status, default: 'pending'
    end

    change_table :orb_rankings do |t|
      t.string :status, default: 'pending'
    end

    change_table :orb_technologies do |t|
      t.string :status, default: 'pending'
    end

    change_table :orb_technology_categories do |t|
      t.string :status, default: 'pending'
    end
  end
end
