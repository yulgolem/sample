class AddDownloadsToOrbList < ActiveRecord::Migration[5.0]
  def change
    add_attachment :orb_lists, :csv
    add_attachment :orb_lists, :xls
  end
end
