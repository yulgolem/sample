class RenameResultToParams < ActiveRecord::Migration[5.0]
  def change
      rename_column :orb_lists, :result, :query_params
  end
end
