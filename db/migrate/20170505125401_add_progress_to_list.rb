class AddProgressToList < ActiveRecord::Migration[5.0]
  def change
    change_table :orb_lists do |t|
      t.integer :current_progress, default: 0
    end
  end
end
