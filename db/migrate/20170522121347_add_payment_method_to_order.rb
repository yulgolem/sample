class AddPaymentMethodToOrder < ActiveRecord::Migration[5.0]
  def change
    change_table :orders do |t|
      t.text :payment_method
    end
  end
end
