class DropPaymenyTypeIdFromOrders < ActiveRecord::Migration[5.0]
  def change
    execute 'ALTER TABLE orders DROP COLUMN IF EXISTS payment_method_id;'
  end
end
