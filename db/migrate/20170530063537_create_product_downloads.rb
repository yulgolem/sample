class CreateProductDownloads < ActiveRecord::Migration[5.0]
  def change
    create_table :product_downloads do |t|

      t.belongs_to :user
      t.belongs_to :product
      t.datetime :file_downloaded_at
      t.string :file_downloaded_format

      t.timestamps
    end
  end
end
