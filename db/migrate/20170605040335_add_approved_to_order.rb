class AddApprovedToOrder < ActiveRecord::Migration[5.0]
  def change
    change_table :orders do |t|
      t.boolean :payment_approved, default: false
    end
  end
end
