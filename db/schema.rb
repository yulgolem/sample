# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170605040335) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "authentications", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.string   "provider",   null: false
    t.string   "uid",        null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["provider", "uid"], name: "index_authentications_on_provider_and_uid", using: :btree
  end

  create_table "cart_products", force: :cascade do |t|
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "cart_id"
    t.decimal  "price"
  end

  create_table "carts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "status",     default: "active"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contact_messages", force: :cascade do |t|
    t.string   "email"
    t.string   "name"
    t.string   "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "features", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "fa_icon_id"
  end

  create_table "global_settings", force: :cascade do |t|
    t.string "feedback_target_email"
  end

  create_table "orb_companies", force: :cascade do |t|
    t.integer  "orb_list_id"
    t.json     "response"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "rank"
    t.string   "name"
    t.string   "description"
    t.string   "website"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "country"
    t.string   "revenue"
    t.string   "num_of_emp"
    t.string   "industry"
    t.string   "naics_code"
    t.string   "naics_description"
    t.string   "year_founded"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.index ["orb_list_id"], name: "index_orb_companies_on_orb_list_id", using: :btree
  end

  create_table "orb_industries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "status",     default: "pending"
  end

  create_table "orb_lists", force: :cascade do |t|
    t.string   "title"
    t.integer  "orb_num"
    t.string   "entity_type"
    t.string   "name"
    t.string   "name_prefix"
    t.string   "webdomain"
    t.string   "industry"
    t.string   "naics_codes"
    t.string   "sic_codes"
    t.string   "employees"
    t.string   "revenue"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "country"
    t.integer  "parent_orb_num"
    t.string   "rankings"
    t.string   "technologies"
    t.string   "tech_categories"
    t.string   "importance_score"
    t.integer  "user_id"
    t.json     "query_params"
    t.integer  "result_count"
    t.boolean  "is_file_downloaded"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "status",             default: "pending"
    t.string   "csv_file_name"
    t.string   "csv_content_type"
    t.integer  "csv_file_size"
    t.datetime "csv_updated_at"
    t.string   "xls_file_name"
    t.string   "xls_content_type"
    t.integer  "xls_file_size"
    t.datetime "xls_updated_at"
    t.integer  "current_progress",   default: 0
    t.index ["user_id"], name: "index_orb_lists_on_user_id", using: :btree
  end

  create_table "orb_naics", force: :cascade do |t|
    t.integer  "code"
    t.string   "description"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "status",      default: "pending"
  end

  create_table "orb_rankings", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orb_sics", force: :cascade do |t|
    t.integer  "code"
    t.string   "description"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "status",      default: "pending"
  end

  create_table "orb_technologies", force: :cascade do |t|
    t.string   "name"
    t.string   "category"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "status",     default: "pending"
  end

  create_table "orb_technology_categories", force: :cascade do |t|
    t.string   "name"
    t.text     "technologies",                                  array: true
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "status",       default: "pending"
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "cart_id"
    t.string   "status",           default: "pending"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "articulus"
    t.string   "payment_token"
    t.decimal  "total_price"
    t.string   "transaction_id"
    t.text     "payment_method"
    t.boolean  "payment_approved", default: false
  end

  create_table "pages", force: :cascade do |t|
    t.string   "target"
    t.string   "meta_title"
    t.string   "meta_desc"
    t.string   "meta_keywords"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_downloads", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.datetime "file_downloaded_at"
    t.string   "file_downloaded_format"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["product_id"], name: "index_product_downloads_on_product_id", using: :btree
    t.index ["user_id"], name: "index_product_downloads_on_user_id", using: :btree
  end

  create_table "product_features", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "feature_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feature_id"], name: "index_product_features_on_feature_id", using: :btree
    t.index ["product_id", "feature_id"], name: "index_product_features_on_product_id_and_feature_id", unique: true, using: :btree
    t.index ["product_id"], name: "index_product_features_on_product_id", using: :btree
  end

  create_table "product_subcategories", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "subcategory_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["product_id", "subcategory_id"], name: "index_product_subcategories_on_product_id_and_subcategory_id", unique: true, using: :btree
    t.index ["product_id"], name: "index_product_subcategories_on_product_id", using: :btree
    t.index ["subcategory_id"], name: "index_product_subcategories_on_subcategory_id", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.string   "articulus"
    t.integer  "base_product_id"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
    t.string   "txt_list_file_name"
    t.string   "txt_list_content_type"
    t.integer  "txt_list_file_size"
    t.datetime "txt_list_updated_at"
    t.string   "xls_list_file_name"
    t.string   "xls_list_content_type"
    t.integer  "xls_list_file_size"
    t.datetime "xls_list_updated_at"
    t.string   "csv_list_file_name"
    t.string   "csv_list_content_type"
    t.integer  "csv_list_file_size"
    t.datetime "csv_list_updated_at"
    t.string   "example_list_file_name"
    t.string   "example_list_content_type"
    t.integer  "example_list_file_size"
    t.datetime "example_list_updated_at"
    t.datetime "last_updated_at_pub"
    t.datetime "last_updated_at_internal"
    t.text     "description"
    t.text     "detailed_description"
    t.integer  "list_length"
    t.decimal  "price"
    t.string   "discount"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.text     "list_length_commentary"
    t.boolean  "is_latest_version",         default: true
    t.text     "structure"
    t.decimal  "price_discounted"
    t.boolean  "is_hidden",                 default: false
    t.string   "label_bg_color",            default: "#e27c7c"
    t.string   "meta_title"
    t.string   "meta_desc"
    t.string   "meta_keywords"
  end

  create_table "subcategories", force: :cascade do |t|
    t.string   "title"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["category_id"], name: "index_subcategories_on_category_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "crypted_password"
    t.string   "salt"
    t.boolean  "is_admin",                        default: false
    t.boolean  "is_email_confirmed",              default: false
    t.string   "secret_token"
    t.string   "remember_me_token"
    t.datetime "remember_me_token_expires_at"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_token_expires_at"
    t.datetime "reset_password_email_sent_at"
    t.string   "role",                            default: "user"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "company"
    t.string   "country_code"
    t.string   "activation_state"
    t.string   "activation_token"
    t.datetime "activation_token_expires_at"
    t.index ["activation_token"], name: "index_users_on_activation_token", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", using: :btree
  end

end
