class PaypalInterface

  HOST = Rails.application.secrets.email_host

  PAYPAL_RETURN_URL = "#{HOST}/orders/paypal_success"
  PAYPAL_CANCEL_URL = "#{HOST}/orders/paypal_rejected"
  PAYPAL_NOTIFY_URL = "#{HOST}/notify"

  def initialize(order)
    @order   = order

    if @order.payment_token.present?
      @payment = PayPal::SDK::REST::Payment.find(@order.payment_token)
    else
      @payment = PayPal::SDK::REST::Payment.new({
        :intent => 'sale',

        :payer => {
          :payment_method => 'paypal'
        },

        :redirect_urls => {
          :return_url => PAYPAL_RETURN_URL,
          :cancel_url => PAYPAL_CANCEL_URL
        },

        :transactions => [{
          :amount => {
            :total    => @order.total_price,
            :currency => 'USD'
          },
          :description => "Listoteca.com order ##{@order.articulus}"
        }]
      })

      if @payment.create
        @order.update_attributes(payment_token: @payment.id)
      end
    end
  end

  def redirect_url
    @payment.links.find{|v| v.rel == "approval_url" }.href
  end

  def execute(payer_id)
    @payment.execute(:payer_id => payer_id)
  end
end
