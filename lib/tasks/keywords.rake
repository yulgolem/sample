namespace :keywords do
  task :generate => :environment do
    begin
      prefixes = ['list of', 'list of companies', 'markering list of']

      states = ['Alabama', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut',
                'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana',
                   'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland',
                   'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri',
                   'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey',
                   'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio',
                   'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina',
                   'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia',
                   'Washington', 'West Virginia', 'Wisconsin', 'Wyoming']

      industries = Orb::Industry.all

      result = File.open(Rails.root + 'tmp/keywords.txt', 'w')

      prefixes.each do |prefix|
        states.each do |state|
          industries.each do |industry|

            result.write("#{prefix} #{state} #{industry.name} \n\r")
          end
        end
      end

    rescue
      p $!
    ensure
      result.close unless result.nil?
    end
  end
end

