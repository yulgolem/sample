require 'rest-client'

namespace :orb do
  API_HOST = "http://api2.orb-intelligence.com/2"
  API_KEY  = ""

  task :companies => :environment do
    begin
      result = RestClient.get("#{API_HOST}/search/companies?api_key=#{API_KEY}&naics_codes=927&limit=100")

      json = JSON.parse(result.body)
      p json['result_count']
      json['result_set'].each { |c| p c['name']}
    rescue
      p $!
    end
  end

  task :naic => :environment do
    begin
      result = RestClient.get("#{API_HOST}/dictionaries/naics_codes?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |naic|
        orb_naic = Orb::Naic.find_or_create_by(code: naic['code'].to_i)
        orb_naic.update_attribute(:description, naic['description'])
      end
    rescue
      p $!
    end
  end

  task :industries => :environment do
    begin
      result = RestClient.get("#{API_HOST}/dictionaries/industries?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |industry|
        orb_industry = Orb::Industry.find_or_create_by(name: industry['name'])
      end
    rescue
      p $!
    end
  end

  task :sic => :environment do
    begin
      result = RestClient.get("#{API_HOST}/dictionaries/sic_codes?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |sic|
        orb_sic = Orb::Sic.find_or_create_by(code: sic['code'].to_i)
        orb_sic.update_attribute(:description, sic['description'])
      end
    rescue
      p $!
    end
  end

  task :technologies => :environment do
    begin
      result = RestClient.get("#{API_HOST}/dictionaries/technologies?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |technology|
        orb_technology = Orb::Technology.find_or_create_by(name: technology['name'])
        orb_technology.update_attribute(:category, technology['category'])
      end
    rescue
      p $!
    end
  end

  task :technology_categories => :environment do
    begin
      result = RestClient.get("#{API_HOST}/dictionaries/technologies/categories?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |category|
        orb_technology_category = Orb::TechnologyCategory.find_or_create_by(name: category['name'])
        orb_technology_category.update_attribute(:technologies, category['technologies'])
      end
    rescue
      p $!
    end
  end

  task :rankings => :environment do
    begin
      result = RestClient.get("#{API_HOST}/dictionaries/rankings?api_key=#{API_KEY}")
      json = JSON.parse(result.body)
      json.each do |ranking|
        orb_ranking = Orb::Ranking.find_or_create_by(name: ranking['name'])
      end
    rescue
      p $!
    end
  end
end
