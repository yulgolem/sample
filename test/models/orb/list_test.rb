require 'test_helper'

class Orb::ListTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end

# == Schema Information
#
# Table name: orb_lists
#
#  id                 :integer          not null, primary key
#  title              :string
#  orb_num            :integer
#  entity_type        :string
#  name               :string
#  name_prefix        :string
#  webdomain          :string
#  industry           :string
#  naics_codes        :string
#  sic_codes          :string
#  employees          :string
#  revenue            :string
#  city               :string
#  state              :string
#  zip                :string
#  country            :string
#  parent_orb_num     :integer
#  rankings           :string
#  technologies       :string
#  tech_categories    :string
#  importance_score   :string
#  user_id            :integer
#  query_params       :json
#  result_count       :integer
#  is_file_downloaded :boolean
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  status             :string           default("pending")
#  csv_file_name      :string
#  csv_content_type   :string
#  csv_file_size      :integer
#  csv_updated_at     :datetime
#  xls_file_name      :string
#  xls_content_type   :string
#  xls_file_size      :integer
#  xls_updated_at     :datetime
#  current_progress   :integer          default(0)
#
