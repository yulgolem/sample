require 'test_helper'

class Orb::TechnologyCategoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end

# == Schema Information
#
# Table name: orb_technology_categories
#
#  id           :integer          not null, primary key
#  name         :string
#  technologies :text             is an Array
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  status       :string           default("pending")
#
